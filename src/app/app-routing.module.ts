import { HeadOfDepartementModule } from './head-of-departement/head-of-departement.module';
import { EtAdminComponent } from './administration/et-admin/et-admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule) },
  { path: 'et-admin', loadChildren: () => import('./administration/administration.module').then(m => m.AdministrationModule) },
  { path: 'head-of-department', loadChildren: () => import('./head-of-departement/head-of-departement.module').then(m => m.HeadOfDepartementModule) },
  { path: 'head-of-class', loadChildren: () => import('./head-class-room/head-class-room.module').then(m => m.HeadClassRoomModule) },
  { path: 'teacher', loadChildren: () => import('./teacher/teacher.module').then(m => m.TeacherModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
