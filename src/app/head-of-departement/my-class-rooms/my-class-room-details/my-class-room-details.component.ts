import { StatClassRoom } from './../../../models/stat-class-room';
import { ShowSeancesComponent } from './show-seances/show-seances.component';
import { ProCalendar } from './../../../models/pro-calendar';
import { ShowProcalendarComponent } from './show-procalendar/show-procalendar.component';
import { AddProCalendarComponent } from './add-pro-calendar/add-pro-calendar.component';
import { EditProgrammeComponent } from './edit-programme/edit-programme.component';
import { RemoveProgrammeComponent } from './remove-programme/remove-programme.component';
import { ModalAlertComponent } from './../../modal-alert/modal-alert.component';
import { AddProgrammeComponent } from './add-programme/add-programme.component';
import { RessourceService } from './../../../administration/services/ressource.service';
import { ActorService } from './../../../administration/services/actor.service';
import { ProgrammeCourse } from './../../../models/programme-course';
import { ClassRoom } from './../../../models/class-room';
import { Field } from './../../../models/field';
import { DepartmentService } from './../../services/department.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  selector: 'app-my-class-room-details',
  templateUrl: './my-class-room-details.component.html',
  styleUrls: ['./my-class-room-details.component.scss']
})
export class MyClassRoomDetailsComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Mes classes",
      link: "head-of-department/my-class-rooms"
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  isLoadingProgrammation: boolean = false;
  isLoadingTaskTime: boolean = false;
  currentField : Field;
  currentClassRoom: ClassRoom;
  programmeCourses: Array<ProgrammeCourse> = new Array<ProgrammeCourse>();
  proCalendars: Array<ProCalendar> = new Array<ProCalendar>();
  statClassRoom : StatClassRoom = new StatClassRoom();


  constructor(public dialog: MatDialog, public departmentService: DepartmentService,
    private route: ActivatedRoute, private actorService: ActorService, private ressourceService: RessourceService) {

     }

  ngOnInit(): void {
    // this.route.snapshot.paramMap.get('id')
    this.getCurrentField();
    this.fetchCurrentClass();

  }

  getAllProCalendars(event: MatTabChangeEvent){
    // console.log(event);
    if (event.index == 1) {
      this.isLoadingTaskTime = true;
      this.departmentService.getAllProCalendarField(this.currentClassRoom.id).subscribe(
        (response)=>{
          console.log(response);
          this.proCalendars = response;
          this.isLoadingTaskTime = false;
        }
      );
    }
  }

  definedProgramme(){
    const dialogRef = this.dialog.open(AddProgrammeComponent, {
      width: "700px",
      data: {
        classRoomId: this.currentClassRoom.id
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: ProgrammeCourse) => {
      if (result) {
        this.programmeCourses.push(result);
      }
    });
  }

  editProgrammeCourse(programmeCourse: ProgrammeCourse){
    const dialogRef = this.dialog.open(EditProgrammeComponent, {
      width: "700px",
      data: programmeCourse,
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: ProgrammeCourse) => {
      if (result) {
        const index = this.programmeCourses.findIndex((elmt) => {
          return (elmt.id == programmeCourse.id);
        });
        this.programmeCourses[index] = result;
      }
    });
  }

  definedProCalendar(programmeCourse: ProgrammeCourse){
    const dialogRef = this.dialog.open(AddProCalendarComponent, {
      width: "700px",
      data: {
        programmeCourseId: programmeCourse.id
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: ProgrammeCourse) => {
      if (result) {
        console.log(result);
      }
    });
  }

  showSeance(programmeCourse: ProgrammeCourse){
    const dialogRef = this.dialog.open(ShowSeancesComponent, {
      width: "900px",
      data: {
        subjectFieldId: programmeCourse.id,
      },
      autoFocus: false,
      disableClose: true,
    });

    // dialogRef.afterClosed().subscribe((result: ProgrammeCourse) => {
    //   if (result) {
    //   }
    // });
  }

  showProCalendar(programmeCourse: ProgrammeCourse){
    const dialogRef = this.dialog.open(ShowProcalendarComponent, {
      width: "700px",
      data: {
        programmeCourseId: programmeCourse.id,
        classRoomId: this.currentClassRoom.id
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: ProgrammeCourse) => {
      if (result) {
        // console.log(result);
      }
    });
  }


  makeActiveProgrammeCourse(programmeCourse: ProgrammeCourse){
    this.departmentService.makeStarted(programmeCourse.id).subscribe(
      (response)=>{
        const index = this.programmeCourses.findIndex((elmt) => {
          return (elmt.id == programmeCourse.id);
        });
        this.programmeCourses[index].started = true;
      }
    );
  }

  removeProgrammeCourse(programmeCourse: ProgrammeCourse){
    const alertDialogRef = this.dialog.open(ModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment supprimer cette programmation?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(RemoveProgrammeComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ programmeCourseId: programmeCourse.id})),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const index = this.programmeCourses.findIndex((elmt) => {
              return (elmt.id == programmeCourse.id);
            });
            this.programmeCourses.splice(index!, 1);
          }
        });
      }
    });
  }

  fetchProgrammeCourse(){
    this.isLoadingProgrammation = true;
    this.departmentService.getProgrammeCourses(this.currentClassRoom.id).subscribe(
      (response)=>{
        this.programmeCourses = response;
        this.isLoadingProgrammation = false;
      },
      (error)=>{

      }
    );
  }

  getCurrentField(){
    this.departmentService.getCurrentField()
      .subscribe(arg => {
        this.currentField = arg;
        // this.fetchMyClass();
    });
  }

  fetchCurrentClass(){
    this.departmentService.getOneClassRoom(+this.route.snapshot.paramMap.get('id')!).subscribe(
      (response)=>{
        this.currentClassRoom = response;
        this.fetchProgrammeCourse();
        this.fetchStatByClass();
        // console.log(response);
        this.linkTree.push({
          name: this.currentClassRoom.name,
          link: `head-of-department/my-class-rooms/details/${this.route.snapshot.paramMap.get('id')}`,
        });
      }
    );
  }

  fetchStatByClass(){
    this.departmentService.getStatClassRoom(this.currentClassRoom.id).subscribe(
      (response)=>{
        this.statClassRoom = response;
      }
    );
  }

  relaod() {
    this.isReloading = true;
    this.getCurrentField();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
