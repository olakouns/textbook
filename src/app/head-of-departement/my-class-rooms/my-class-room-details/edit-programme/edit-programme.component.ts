import { ProgrammeCourse } from './../../../../models/programme-course';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DepartmentService } from './../../../services/department.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-edit-programme',
  templateUrl: './edit-programme.component.html',
  styleUrls: ['./edit-programme.component.scss']
})
export class EditProgrammeComponent implements OnInit {

  title: String = "Modifier la programmation";
  validateText: String = "Valider";
  form: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode: String;
  error: boolean = false;
  errorMessage: string = "";
  programmeCourse: ProgrammeCourse;

  constructor(private formBuilder: FormBuilder,
    private departmentService: DepartmentService,
    public dialogRef: MatDialogRef<EditProgrammeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProgrammeCourse) {

      this.programmeCourse = Object.assign({}, this.data);
      // this.programmeCourse = this.data;
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      startedDate: ['', Validators.required],
      endedDate: ['', Validators.required],
      duration: ['', Validators.required],
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;

    this.departmentService.updateProgrammeCourse(this.programmeCourse.id, this.programmeCourse).subscribe(
      (response)=>{
        this.dialogRef.close(response);
        this.isLoading = false;
      },
      (error)=>{
        this.isLoading = false;
        this.error = true;
        this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
      }
    );
  }

}
