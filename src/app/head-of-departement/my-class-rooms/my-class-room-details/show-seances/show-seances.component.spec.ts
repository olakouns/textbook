import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowSeancesComponent } from './show-seances.component';

describe('ShowSeancesComponent', () => {
  let component: ShowSeancesComponent;
  let fixture: ComponentFixture<ShowSeancesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowSeancesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSeancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
