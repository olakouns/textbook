import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Seance } from './../../../../models/seance';
import { Page } from './../../../../payload/page';
import { DepartmentService } from './../../../services/department.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';

@Component({
  selector: 'app-show-seances',
  templateUrl: './show-seances.component.html',
  styleUrls: ['./show-seances.component.scss']
})
export class ShowSeancesComponent implements OnInit {

  title: String = "Liste des seances effectuées";
  validateText: String = "OK";
  isLoading: boolean = false;

  isTimeout: boolean = false;
  isReloading: boolean = false;
  subjectFieldId : string;
  search: string = "";
  page: number = 0;
  size: number = 10;

  seancesPage: Page<Seance> = new Page<Seance>();
  seances: Array<Seance> = new Array<Seance>();
  displayedColumns: string[] = ['day', 'hours',  'workDone',  'validate'];
  dataSource : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialogRef: MatDialogRef<ShowSeancesComponent>,
    private departmentService: DepartmentService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.subjectFieldId = this.data.subjectFieldId;
     }

  ngOnInit(): void {
    this.fetchSeance();
  }

  close() {
    this.dialogRef.close();
  }

  fetchSeance(){
    this.isLoading = true;
    this.departmentService.getSeance(this.subjectFieldId, this.search, this.page, this.size).subscribe(
      (response)=>{
        this.seancesPage = response;
        this.seances = this.seancesPage.content;
        this.dataSource = new MatTableDataSource<Seance>(this.seances);
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
        // console.log(this.seances);
      },
      (error)=>{

      }
    );
  }

}
