import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowProcalendarComponent } from './show-procalendar.component';

describe('ShowProcalendarComponent', () => {
  let component: ShowProcalendarComponent;
  let fixture: ComponentFixture<ShowProcalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowProcalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowProcalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
