import { TypeDay } from './../../../../models/type-day.enum';
import { ProCalendar } from './../../../../models/pro-calendar';
import { DepartmentService } from './../../../services/department.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-show-procalendar',
  templateUrl: './show-procalendar.component.html',
  styleUrls: ['./show-procalendar.component.scss']
})
export class ShowProcalendarComponent implements OnInit {

  title: String = "Liste des honoraires";
  validateText: String = "OK";
  isLoading: boolean = false;
  proCalendars: Array<ProCalendar> = new Array<ProCalendar>();

  constructor(public dialogRef: MatDialogRef<ShowProcalendarComponent>,
    private departmentService: DepartmentService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {

    this.fecthProCalendar();
  }

  close() {
    this.dialogRef.close();
  }

  fecthProCalendar(){
    this.departmentService.getProCalendarField(this.data.programmeCourseId, this.data.classRoomId).subscribe(
      (response)=>{
        this.proCalendars = response;
      }
    )
  }

  formatValue(value: any){
    let out = "";
    switch (value) {
      case TypeDay.MONDAY:
        out =  "Lundi";
        break;
      case TypeDay.TUESDAY:
        out =  "Mardi";
        break;
      case TypeDay.WEDNESDAY:
        out =  "Mercredi";
        break;
      case TypeDay.THURSDAY:
        out =  "Jeudi";
        break;
      case TypeDay.FRIDAY:
        out =  "Vendredi";
        break;
      case TypeDay.SATURDAY:
        out =  "Samedi";
        break;

      default:
        break;
    }
    return out;
  }


}
