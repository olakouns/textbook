import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProCalendarComponent } from './add-pro-calendar.component';

describe('AddProCalendarComponent', () => {
  let component: AddProCalendarComponent;
  let fixture: ComponentFixture<AddProCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddProCalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
