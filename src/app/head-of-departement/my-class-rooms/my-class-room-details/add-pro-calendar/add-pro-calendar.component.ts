import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DepartmentService } from './../../../services/department.service';
import { TypeDay } from './../../../../models/type-day.enum';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProCalendar } from './../../../../models/pro-calendar';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-add-pro-calendar',
  templateUrl: './add-pro-calendar.component.html',
  styleUrls: ['./add-pro-calendar.component.scss']
})
export class AddProCalendarComponent implements OnInit {

  title: String = "Faire une programmation";
  validateText: String = "Valider";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode: String;
  error: boolean = false;
  errorMessage: string = "";
  proCalendar: ProCalendar;
  dayValue = [
    {
      key :  TypeDay.MONDAY,
      value: "Lundi"
    },
    {
      key :  TypeDay.TUESDAY,
      value: "Mardi"
    },
    {
      key :  TypeDay.WEDNESDAY,
      value: "Mercredi"
    },
    {
      key :  TypeDay.THURSDAY,
      value: "Jeudi"
    },
    {
      key :  TypeDay.FRIDAY,
      value: "Vendredi"
    },
    {
      key :  TypeDay.SATURDAY,
      value: "Samedi"
    }
  ];

  startedHoursValue= "08:00";
  endedHoursValue= "08:00";


  constructor(private formBuilder: FormBuilder,
    private departmentService: DepartmentService,
    public dialogRef: MatDialogRef<AddProCalendarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.proCalendar = new ProCalendar();
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      typeDay: ['', Validators.required],
      startedHours: ['', Validators.required],
      endedHours: ['', Validators.required],
      duration: ['', Validators.required],
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    this.proCalendar.startedHours = new Date("Tue Jun 15 2021 "+this.startedHoursValue+":00 GMT+0100");
    this.proCalendar.endedHours = new Date("Tue Jun 15 2021 "+this.endedHoursValue+":00 GMT+0100");
    this.departmentService.makeProCalendar(this.data.programmeCourseId, this.proCalendar).subscribe(
      (response)=>{
        this.dialogRef.close(response);
        this.isLoading = false;
      },
      (error)=>{
        this.isLoading = false;
        this.error = true;
        this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
      }
    );
  }

}
