import { DepartmentService } from './../../../services/department.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../../../administration/services/ressource.service';
import { ActorService } from './../../../../administration/services/actor.service';
import { ProgrammeCourse } from './../../../../models/programme-course';
import { SubjectField } from './../../../../models/subject-field';
import { Actor } from './../../../../models/actor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-add-programme',
  templateUrl: './add-programme.component.html',
  styleUrls: ['./add-programme.component.scss']
})
export class AddProgrammeComponent implements OnInit {
  title: String = "Faire une programmation";
  validateText: String = "Valider";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode: String;
  error: boolean = false;
  errorMessage: string = "";
  teachers: Array<Actor> = new Array<Actor>();
  subjectFields: Array<SubjectField> = new Array<SubjectField>();
  programmeCourse : ProgrammeCourse ;
  show = false;
  search: string = "";
  actorId :  string;
  actorIsSelected: boolean = false;
  isReloading: boolean = false;
  actorSubscription: Subscription;
  teacherId: string;
  subjectFieldId: string;
  classRoomId: number;

  constructor(private formBuilder: FormBuilder,
    private actorService : ActorService,
    private departmentService: DepartmentService,
    private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<AddProgrammeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

      this.programmeCourse = new ProgrammeCourse();
      this.classRoomId = this.data.classRoomId;
    }

  ngOnInit(): void {
    this.fecthSubjectField();

    this.form = this.formBuilder.group({
      subjectField: ['', Validators.required],
      prof: ['', Validators.required],
      startedDate: ['', Validators.required],
      endedDate: ['', Validators.required],
      duration: ['', Validators.required],
    });
  }


  onSubmit(){
    this.error = false;
    this.isLoading = true;
    this.departmentService.makeProgrammeCourse(this.teacherId, this.subjectFieldId, this.classRoomId, this.programmeCourse).subscribe(
      (response)=>{
        this.dialogRef.close(response);
        this.isLoading = false;
      },
      (error)=>{
        this.isLoading = false;
        this.error = true;
        this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
      }
    );
  }


  filterActor(event: any) {
    if (this.search.length >= 1) {
      this.teachers = [];
      // return;
    }
    this.actorSubscription = this.actorService.getAllTeacher(this.search, 0, 10).subscribe(
      (response) => {
        this.teachers = response.content;
      },
      (error) => {
        this.catchError(error);
      }
    );
  }

  selectActor(event: MatAutocompleteSelectedEvent) {
    this.teacherId = event.option.value.id;
    this.search = event.option.value.firstName + ' ' + event.option.value.lastName ;
    this.actorIsSelected = true;
  }

  catchError(error : any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

  fecthSubjectField(){
    this.departmentService.getSubjectField()
      .subscribe(arg => this.subjectFields = arg);
  }


}
