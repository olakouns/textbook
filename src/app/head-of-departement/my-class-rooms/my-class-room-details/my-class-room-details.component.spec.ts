import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyClassRoomDetailsComponent } from './my-class-room-details.component';

describe('MyClassRoomDetailsComponent', () => {
  let component: MyClassRoomDetailsComponent;
  let fixture: ComponentFixture<MyClassRoomDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyClassRoomDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyClassRoomDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
