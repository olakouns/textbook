import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DepartmentService } from './../../../services/department.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-remove-programme',
  templateUrl: './remove-programme.component.html',
  styleUrls: ['./remove-programme.component.scss']
})
export class RemoveProgrammeComponent implements OnInit {

  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private departmentService : DepartmentService,
    public dialogRef: MatDialogRef<RemoveProgrammeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.removeProgrammeCourse();
  }

  removeProgrammeCourse() {
    this.departmentService
      .deleteProgramming(this.data.programmeCourseId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }

}
