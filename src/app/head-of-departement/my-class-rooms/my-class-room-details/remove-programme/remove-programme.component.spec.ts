import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveProgrammeComponent } from './remove-programme.component';

describe('RemoveProgrammeComponent', () => {
  let component: RemoveProgrammeComponent;
  let fixture: ComponentFixture<RemoveProgrammeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveProgrammeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
