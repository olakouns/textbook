import { StatProgram } from './../../../../models/stat-program';
import { DepartmentService } from './../../../services/department.service';
import { ProgrammeCourse } from './../../../../models/programme-course';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-programm-item',
  templateUrl: './programm-item.component.html',
  styleUrls: ['./programm-item.component.scss']
})
export class ProgrammItemComponent implements OnInit , OnChanges{

  @Input() programmeCourse: ProgrammeCourse;
  @Output() editProgrammeCourse: EventEmitter<void> = new EventEmitter<void>();
  @Output() removeProgrammeCourse: EventEmitter<void> = new EventEmitter<void>();
  @Output() definedProCalendar: EventEmitter<void> = new EventEmitter<void>();
  @Output() makeActiveProgrammeCourse: EventEmitter<void> = new EventEmitter<void>();
  @Output() showProCalendar: EventEmitter<void> = new EventEmitter<void>();
  @Output() showSeance: EventEmitter<void> = new EventEmitter<void>();
  isChecked: boolean = false;
  statProgram : StatProgram = new StatProgram();
  load = false;

  constructor(private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.isChecked = this.programmeCourse?.started;
    this.fetchStatProgram();
  }


  ngOnChanges(changes: SimpleChanges): void {
    // console.log("ok");
    // if(changes.isChecked.currentValue){
    //   console.log("ok");
    //   this.makeActiveProgrammeCourse.emit();
    // }
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

  }

  onToggle(event : MatSlideToggleChange){
    console.log(this.isChecked);
    if (this.isChecked) {
      this.makeActiveProgrammeCourse.emit();
    }
  }

  fetchStatProgram(){
    this.load = true;
    this.departmentService.getTimesDone(this.programmeCourse.id).subscribe(
      (response)=>{
          this.statProgram = response;
          this.load = false;
      },
      (error)=>{

      }
    );
  }

}
