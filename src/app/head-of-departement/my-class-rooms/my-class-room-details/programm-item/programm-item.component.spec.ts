import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammItemComponent } from './programm-item.component';

describe('ProgrammItemComponent', () => {
  let component: ProgrammItemComponent;
  let fixture: ComponentFixture<ProgrammItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgrammItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
