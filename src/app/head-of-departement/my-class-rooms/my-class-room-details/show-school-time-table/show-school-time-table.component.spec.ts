import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowSchoolTimeTableComponent } from './show-school-time-table.component';

describe('ShowSchoolTimeTableComponent', () => {
  let component: ShowSchoolTimeTableComponent;
  let fixture: ComponentFixture<ShowSchoolTimeTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowSchoolTimeTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSchoolTimeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
