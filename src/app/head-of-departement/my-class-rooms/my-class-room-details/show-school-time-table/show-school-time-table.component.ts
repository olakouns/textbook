import { TypeDay } from './../../../../models/type-day.enum';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ProCalendar } from 'src/app/models/pro-calendar';

@Component({
  selector: 'app-show-school-time-table',
  templateUrl: './show-school-time-table.component.html',
  styleUrls: ['./show-school-time-table.component.scss']
})
export class ShowSchoolTimeTableComponent implements OnInit {

  @Input() proCalendars: Array<ProCalendar> = new Array<ProCalendar> ();
  dayValue = [
    {
      key :  TypeDay.MONDAY,
      value: "Lundi"
    },
    {
      key :  TypeDay.TUESDAY,
      value: "Mardi"
    },
    {
      key :  TypeDay.WEDNESDAY,
      value: "Mercredi"
    },
    {
      key :  TypeDay.THURSDAY,
      value: "Jeudi"
    },
    {
      key :  TypeDay.FRIDAY,
      value: "Vendredi"
    },
    {
      key :  TypeDay.SATURDAY,
      value: "Samedi"
    }
  ];
  @Input() showProCalendars: ProCalendar;
  constructor() { }

  ngOnInit(): void {
  }



  refactoring(proCalendar: Array<ProCalendar>, day: TypeDay){
    let value = new Array<ProCalendar>();
    proCalendar.forEach(element => {
      if (element.typeDay == day ) {
        value.push(element);
      }
    });

    return value;

  }

  formatValue(value: any){
    let out = "";
    switch (value) {
      case TypeDay.MONDAY:
        out =  "Lundi";
        break;
      case TypeDay.TUESDAY:
        out =  "Mardi";
        break;
      case TypeDay.WEDNESDAY:
        out =  "Mercredi";
        break;
      case TypeDay.THURSDAY:
        out =  "Jeudi";
        break;
      case TypeDay.FRIDAY:
        out =  "Vendredi";
        break;
      case TypeDay.SATURDAY:
        out =  "Samedi";
        break;

      default:
        break;
    }
    return out;
  }

}
