import { Actor } from './../../models/actor';
import { RessourceService } from './../../administration/services/ressource.service';
import { MatDialog } from '@angular/material/dialog';
import { AddRepresentativeComponent } from './add-representative/add-representative.component';
import { Years } from './../../models/years.enum';
import { Field } from './../../models/field';
import { DepartmentService } from './../services/department.service';
import { Component, OnInit } from '@angular/core';
import { ClassRoom } from 'src/app/models/class-room';

@Component({
  selector: 'app-my-class-rooms',
  templateUrl: './my-class-rooms.component.html',
  styleUrls: ['./my-class-rooms.component.scss']
})
export class MyClassRoomsComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Mes classes",
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  currentField : Field;
  classRooms: Array<ClassRoom> = [];

  yearValue = [
    {
      key : Years.THIRST_YEAR,
      value : "Première année"
    },
    {
      key : Years.SECOND_YEAR,
      value : "Deuxième"
    },
    {
      key : Years.THIRD_YEAR,
      value : "Troisième"
    }
  ];

  constructor(private departmentService : DepartmentService,
    public dialog: MatDialog,
    private ressourceService: RessourceService ) { }

  ngOnInit(): void {
    this.getCurrentField();
  }

  getCurrentField(){
    this.departmentService.getCurrentField()
      .subscribe(arg => {
        this.currentField = arg;
        this.fetchMyClass();
      });
  }

  addClassRoomResp(classRoom: ClassRoom){
    const dialogRef = this.dialog.open(AddRepresentativeComponent, {
      width: "700px",
      data : {
        field: this.currentField,
        classRoom: classRoom
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Actor) => {
      if (result) {
        const indexClass = this.classRooms?.findIndex((elmt) => {
          return (elmt.id == classRoom.id);
        });
        if (indexClass != -1) {

          if(this.classRooms[indexClass].classRepresentative == null) {
            this.classRooms![indexClass].classRepresentative = [];
          }

          this.classRooms[indexClass].classRepresentative.push(result);
        }

      }
    });
  }


  fetchMyClass(){
    this.departmentService.getClassRoom(this.currentField.id).subscribe(
      (response)=>{
        this.classRooms = response;
        // console.log(response);
      }
    );
  }

  relaod() {
    this.isReloading = true;
    this.getCurrentField();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
