import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActorService } from './../../../administration/services/actor.service';
import { RessourceService } from './../../../administration/services/ressource.service';
import { AddActor } from './../../../payload/add-actor';
import { Field } from './../../../models/field';
import { ClassRoom } from './../../../models/class-room';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { Actor } from 'src/app/models/actor';

@Component({
  selector: 'app-add-representative',
  templateUrl: './add-representative.component.html',
  styleUrls: ['./add-representative.component.scss']
})
export class AddRepresentativeComponent implements OnInit {

  title: String = "Definir le responsable de la classe";
  validateText: String = "Ajouter";
  form: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode!: String;
  error: boolean = false;
  errorMessage: string = "";
  classRoom: ClassRoom;
  field: Field;
  addActor: AddActor = new AddActor();
  respo: Actor;

  constructor(private formBuilder: FormBuilder,
    private ressourceService : RessourceService,
    private actorService : ActorService,
    public dialogRef: MatDialogRef<AddRepresentativeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.field = this.data.field;
      this.classRoom = this.data.classRoom;

      this.respo = new Actor();
      this.addActor = new AddActor();
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      email: ['', Validators.required],
      title: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      password: this.mode == "edit" ? [''] : ['', Validators.required]
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    this.actorService.addClassRepresentative(this.addActor) .subscribe(
      (res: Actor) => {
        this.respo = res;
        this.ressourceService.addClassRepresentative(this.classRoom.id, res.id).subscribe(
          (response)=>{
            this.dialogRef.close(res);
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.error = true;
            this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
          }
        );
      },
      (err) => {
        this.isLoading = false;
        this.error = true;
        this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
    });
  }

}
