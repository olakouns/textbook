import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyClassRoomItemComponent } from './my-class-room-item.component';

describe('MyClassRoomItemComponent', () => {
  let component: MyClassRoomItemComponent;
  let fixture: ComponentFixture<MyClassRoomItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyClassRoomItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyClassRoomItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
