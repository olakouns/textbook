import { Router } from '@angular/router';
import { Years } from './../../../models/years.enum';
import { ClassRoom } from './../../../models/class-room';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-my-class-room-item',
  templateUrl: './my-class-room-item.component.html',
  styleUrls: ['./my-class-room-item.component.scss']
})
export class MyClassRoomItemComponent implements OnInit {

  @Input() classRoom: ClassRoom;
  @Output() editClassRoom: EventEmitter<void> = new EventEmitter<void>();
  @Output() addClassRoomResp: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteClassRoom: EventEmitter<void> = new EventEmitter<void>();

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  formatYear(){
    let value = "";
    switch (this.classRoom.years) {
      case Years.THIRST_YEAR:
        value = "Première";
        break;
      case Years.SECOND_YEAR:
        value = "Deuxième";
        break;
      case Years.THIRD_YEAR:
        value = "Troisième";
        break;

      default:
        break;
    }

    return value;
  }

  openDetails(){
    this.router.navigateByUrl(`head-of-department/my-class-rooms/details/${this.classRoom.id}`);
  }

}
