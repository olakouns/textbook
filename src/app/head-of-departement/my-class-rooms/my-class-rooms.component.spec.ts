import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyClassRoomsComponent } from './my-class-rooms.component';

describe('MyClassRoomsComponent', () => {
  let component: MyClassRoomsComponent;
  let fixture: ComponentFixture<MyClassRoomsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyClassRoomsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyClassRoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
