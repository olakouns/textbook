import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammCourseComponent } from './programm-course.component';

describe('ProgrammCourseComponent', () => {
  let component: ProgrammCourseComponent;
  let fixture: ComponentFixture<ProgrammCourseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgrammCourseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
