import { TimesTables } from './../../models/times-tables';
import { ProCalendar } from './../../models/pro-calendar';
import { ClassRoom } from './../../models/class-room';
import { Years } from './../../models/years.enum';
import { ActorService } from './../../administration/services/actor.service';
import { RessourceService } from './../../administration/services/ressource.service';
import { MatDialog } from '@angular/material/dialog';
import { Field } from './../../models/field';
import { DepartmentService } from './../services/department.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-programm-course',
  templateUrl: './programm-course.component.html',
  styleUrls: ['./programm-course.component.scss']
})
export class ProgrammCourseComponent implements OnInit {
  linkTree: Array<any> = [
    {
      name: "Emploi du temps",
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  currentField : Field;
  classRooms: Array<ClassRoom> = [];

  timesTables: Array<TimesTables> = new Array<TimesTables>();

  yearValue = [
    {
      key : Years.THIRST_YEAR,
      value : "Première année"
    },
    {
      key : Years.SECOND_YEAR,
      value : "Deuxième année"
    },
    {
      key : Years.THIRD_YEAR,
      value : "Troisième année"
    }
  ];

  constructor(private departmentService : DepartmentService,
    public dialog: MatDialog, private ressourceService: RessourceService,
    private actorService: ActorService) { }

  ngOnInit(): void {
    this.getCurrentField();
    this.getAllTimesTables();
  }

  getCurrentField(){
    this.departmentService.getCurrentField()
      .subscribe(arg => {
        this.currentField = arg;
        this.fetchMyClass();
      });
  }

  getAllTimesTables(){
    this.isLoading = true;
    this.departmentService.getAllTimesTables().subscribe(
      (response)=>{
        this.timesTables = response;
        this.isLoading = false;
      }
    );
  }

  fetchMyClass(){
    this.departmentService.getClassRoom(this.currentField.id).subscribe(
      (response)=>{
        this.classRooms = response;
      }
    );
  }

  definedProgramme(){

  }


  relaod() {
    this.isReloading = true;
    // this.fetchMarket();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
