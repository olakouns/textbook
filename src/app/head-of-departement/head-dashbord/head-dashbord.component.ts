import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-head-dashbord',
  templateUrl: './head-dashbord.component.html',
  styleUrls: ['./head-dashbord.component.scss']
})
export class HeadDashbordComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Tableau de bord",
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }


  relaod() {
    this.isReloading = true;
    // this.fetchMarket();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
