import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadDashbordComponent } from './head-dashbord.component';

describe('HeadDashbordComponent', () => {
  let component: HeadDashbordComponent;
  let fixture: ComponentFixture<HeadDashbordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadDashbordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadDashbordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
