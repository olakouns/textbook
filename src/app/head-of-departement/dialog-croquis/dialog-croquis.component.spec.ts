import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCroquisComponent } from './dialog-croquis.component';

describe('DialogCroquisComponent', () => {
  let component: DialogCroquisComponent;
  let fixture: ComponentFixture<DialogCroquisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogCroquisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCroquisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
