import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dialog-croquis',
  templateUrl: './dialog-croquis.component.html',
  styleUrls: ['./dialog-croquis.component.scss']
})
export class DialogCroquisComponent implements OnInit {
  @Input() title!: String;
  @Input() otherCondition: boolean;
  @Input() validateText!: String;
  @Input() data: any;
  @Input() form!: FormGroup;
  @Input() isLoading!: boolean;
  @Input() isTimeout: boolean = false;
  @Input() isReloading: boolean = false;
  @Output() onReload: EventEmitter<void> = new EventEmitter<void>();
  @Output() onFormSubmited: EventEmitter<any> = new EventEmitter<any>();

  constructor(public dialogRef: MatDialogRef<DialogCroquisComponent>) { }

  ngOnInit(): void {
    // console.log(this.otherCondition);
    if(this.otherCondition == null) {
      this.otherCondition = true;
    }
  }

  close() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.onFormSubmited.emit(this.data);
  }

}
