import { Actor } from './../../models/actor';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatToolbar } from '@angular/material/toolbar';
import { MatSidenav, MatDrawerMode } from '@angular/material/sidenav';
import { isPlatformBrowser } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxPermissionsService } from 'ngx-permissions';
import { BreakpointObserver, MediaMatcher, Breakpoints } from '@angular/cdk/layout';
import { AuthService } from 'src/app/auth.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { ChangeDetectorRef, Component, OnInit, Inject, PLATFORM_ID, Input, EventEmitter, Output, ViewChild, HostListener } from '@angular/core';

@Component({
  selector: 'app-head-croquis',
  templateUrl: './head-croquis.component.html',
  styleUrls: ['./head-croquis.component.scss']
})
export class HeadCroquisComponent implements OnInit {

  @Input() title!: string;
  @Input() linkTree: Array<any> = [];
  @Input() isTimeout = false;
  @Input() isReloading = false;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onReload: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('sidenav', { static: true }) sidenav!: MatSidenav;
  @ViewChild('toolbar', { static: true }) toolbar!: MatToolbar;
  @ViewChild('notificationMenu', { static: true }) trigger!: MatMenuTrigger;
  options = {
    bottom: 0,
    fixed: false,
    top: 0,
  };
  toolbarFullWidth = false;
  links = [
    {
      name: 'Tableau de bord',
      link: '/head-of-department/dashbord',
      icon: 'dashboard',
      permissions: [],
    },
    {
      name: 'Mes classes',
      link: '/head-of-department/my-class-rooms',
      icon: 'myClass',
      permissions: [],
    },
    {
      name: 'Matières',
      link: '/head-of-department/field-subjects',
      icon: 'matière',
      permissions: [],
    },
    {
      name: 'Emploi du temps',
      link: '/head-of-department/programm-course',
      icon: 'programme',
      permissions: [],
      submenus: []
    }
    // {
    //   name: 'Paramètre',
    //   link: '/medom/settings',
    //   icon: 'settings',
    //   submenus: [
    //     {
    //       name: 'Profil',
    //       link: '/medom/settings/roles',
    //       icon: 'programme',
    //       permissions: [],
    //     }
    //     // {
    //     //   name: 'Gestion des Utilisateurs',
    //     //   link: '/medom/settings/users',
    //     //   icon: 'programme',
    //     //   permissions: [],
    //     // },
    //   ],
    //   permissions: [],
    // },

  ];
  isBrowser: boolean;
  isLoadingProfile = false;
  isLoadingNotifications = true;
  // profile: Profile;
  page = 0;
  size = 20;
  search = '';
  nbNotification = 0;

  hasBackdrop = false;
  mode = 'side' as MatDrawerMode; // over

  mobileQuery: MediaQueryList;
  actor : Actor;

  // tslint:disable-next-line:variable-name
  private readonly _mobileQueryListener: () => void;

  constructor(iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    public router: Router,
    public authService: AuthService,
    breakpointObserver: BreakpointObserver,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private permissionsService: NgxPermissionsService,
    /*private notificationService: NotificationService,*/
    @Inject(PLATFORM_ID) platformId: any,
    private snackBar: MatSnackBar) {
      this.isBrowser = isPlatformBrowser(platformId);

      iconRegistry.addSvgIcon(
        'dashboard',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/images/dashboard.svg'
        )
      );
      iconRegistry.addSvgIcon(
        'myClass',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/images/myClass.svg'
        )
      );


      iconRegistry.addSvgIcon(
        'programme',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/images/programme.svg'
        )
      );
      iconRegistry.addSvgIcon(
        'matière',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/images/matière.svg'
        )
      );

      iconRegistry.addSvgIcon(
        'teacher',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/images/teacher.svg'
        )
      );
      iconRegistry.addSvgIcon(
        'settings',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/images/settings.svg'
        )
      );



      breakpointObserver
      .observe([
        Breakpoints.Handset,
        // Breakpoints.Tablet,
        // Breakpoints.Web,
      ])
      .subscribe((result) => {
        if (result.matches) {
          // this.activateHandsetLayout();
          this.mode = 'over';
          this.hasBackdrop = false;
          this.options.fixed = true;
        } else {
          this.mode = 'side';
          this.hasBackdrop = true;
          this.options.fixed = false;
        }
      });

      this.mobileQuery = media.matchMedia('(max-width: 767px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
    if (!this.isBrowser) { return; }
    this.sidenav.openedChange.subscribe((res) => {
      this.toolbarFullWidth = !res;
    });
    this.isLoadingProfile = true;
    this.getMe();
  }

  getMe(){
    this.authService.getMe().subscribe(
      (response)=>{
        this.actor = response;
      }
    );
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  deconnexion(): any {
    this.authService.logout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event : any): any {
    // console.log(event.target.innerWidth);
  }

}
