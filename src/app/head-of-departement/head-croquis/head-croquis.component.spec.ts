import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadCroquisComponent } from './head-croquis.component';

describe('HeadCroquisComponent', () => {
  let component: HeadCroquisComponent;
  let fixture: ComponentFixture<HeadCroquisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadCroquisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadCroquisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
