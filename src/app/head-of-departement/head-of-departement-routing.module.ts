import { MyClassRoomDetailsComponent } from './my-class-rooms/my-class-room-details/my-class-room-details.component';
import { HeadDashbordComponent } from './head-dashbord/head-dashbord.component';
import { MyClassRoomsComponent } from './my-class-rooms/my-class-rooms.component';
import { DashbordComponent } from './../administration/dashbord/dashbord.component';
import { ProgrammCourseComponent } from './programm-course/programm-course.component';
import { SubjectOfFieldComponent } from './subject-of-field/subject-of-field.component';
import { HeadCroquisComponent } from './head-croquis/head-croquis.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: "", redirectTo: 'dashbord' },
  { path: 'dashbord', component: HeadDashbordComponent },
  { path: 'my-class-rooms', component: MyClassRoomsComponent },
  { path: "my-class-rooms/details/:id", component: MyClassRoomDetailsComponent },
  { path: 'field-subjects', component: SubjectOfFieldComponent },
  { path: 'programm-course', component: ProgrammCourseComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeadOfDepartementRoutingModule { }
