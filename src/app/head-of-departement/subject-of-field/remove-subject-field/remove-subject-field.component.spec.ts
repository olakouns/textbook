import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveSubjectFieldComponent } from './remove-subject-field.component';

describe('RemoveSubjectFieldComponent', () => {
  let component: RemoveSubjectFieldComponent;
  let fixture: ComponentFixture<RemoveSubjectFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveSubjectFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveSubjectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
