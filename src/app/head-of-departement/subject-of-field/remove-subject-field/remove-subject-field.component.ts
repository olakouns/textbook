import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DepartmentService } from './../../services/department.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-remove-subject-field',
  templateUrl: './remove-subject-field.component.html',
  styleUrls: ['./remove-subject-field.component.scss']
})
export class RemoveSubjectFieldComponent implements OnInit {

  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private departmentService : DepartmentService,
    public dialogRef: MatDialogRef<RemoveSubjectFieldComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.removeSubjectField();
  }

  removeSubjectField() {
    this.departmentService
      .deleteSubjectField(this.data.subjectFieldId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }

}
