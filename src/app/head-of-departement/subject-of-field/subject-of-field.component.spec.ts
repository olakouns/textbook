import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectOfFieldComponent } from './subject-of-field.component';

describe('SubjectOfFieldComponent', () => {
  let component: SubjectOfFieldComponent;
  let fixture: ComponentFixture<SubjectOfFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubjectOfFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectOfFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
