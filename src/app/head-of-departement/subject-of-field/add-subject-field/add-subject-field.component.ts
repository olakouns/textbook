import { Semester } from './../../../models/semester.enum';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { RessourceService } from './../../../administration/services/ressource.service';
import { SubjectField } from './../../../models/subject-field';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DepartmentService } from './../../services/department.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'src/app/models/subject';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({
  selector: 'app-add-subject-field',
  templateUrl: './add-subject-field.component.html',
  styleUrls: ['./add-subject-field.component.scss']
})
export class AddSubjectFieldComponent implements OnInit {

  title: String = "Ajouter une matière";
  validateText: String = "Ajouter";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingSubject: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode!: String;
  error: boolean = false;
  errorMessage: string = "";
  subjectField: SubjectField = new SubjectField();
  subjects: Array<Subject> = new Array<Subject>();
  filteredSubjects: Array<Subject> = new Array<Subject>();
  search: string = "";
  subjectSubscription: Subscription;
  isReloading: boolean = false;
  subjectId :  string;
  subjectIsSelected: boolean = false;

  semestreValue = [
    {
      key: Semester.SEMESTER_1,
      value : "Semestre 1"
    },
    {
      key: Semester.SEMESTER_2,
      value : "Semestre 2"
    },
    {
      key: Semester.SEMESTER_3,
      value : "Semestre 3"
    },
    {
      key: Semester.SEMESTER_4,
      value : "Semestre 4"
    },
    {
      key: Semester.SEMESTER_5,
      value : "Semestre 5"
    },
    {
      key: Semester.SEMESTER_6,
      value : "Semestre 6"
    }
  ]

  constructor(private departmentService : DepartmentService,
    private formBuilder: FormBuilder,
    private ressourceService : RessourceService,
    public dialogRef: MatDialogRef<AddSubjectFieldComponent>) { }

  ngOnInit(): void {
    this.buildForm();
  }


  buildForm() {
    this.form = this.formBuilder.group({
      name: ["", [Validators.required]],
      semester: ["", [Validators.required]],
    });
  }

  onSubmit() {
    this.error = false;
    this.isLoading = true;
    this.departmentService.addSubjectField(this.subjectId, this.subjectField.semester, this.subjectField).subscribe(
      (response)=>{
        this.dialogRef.close(response);
      },
      (error) =>{
        this.error = true;
        // this.errorMessage = "Ce producteur a déjà été ajouté";
      }
    );
  }

  filterSubjectField(event: any) {
    // console.log("ok");
    if (this.search.length >= 1) {
      this.subjects = [];
      // return;
    }
    this.subjectSubscription = this.ressourceService.getSubjecs(this.search, 0, 10).subscribe(
      (response) => {
        this.subjects = response.content;
      },
      (error) => {
        this.catchError(error);
      }
    );
  }

  selectSubject(event: MatAutocompleteSelectedEvent) {
    this.subjectId = event.option.value.id;
    this.search = event.option.value.name + ' (' + event.option.value.code + ')';
    this.subjectIsSelected = true;
  }

  catchError(error : any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
