import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubjectFieldComponent } from './add-subject-field.component';

describe('AddSubjectFieldComponent', () => {
  let component: AddSubjectFieldComponent;
  let fixture: ComponentFixture<AddSubjectFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSubjectFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubjectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
