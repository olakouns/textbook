import { RemoveSubjectFieldComponent } from './remove-subject-field/remove-subject-field.component';
import { ModalAlertComponent } from './../modal-alert/modal-alert.component';
import { AddSubjectFieldComponent } from './add-subject-field/add-subject-field.component';
import { RessourceService } from './../../administration/services/ressource.service';
import { MatDialog } from '@angular/material/dialog';
import { SubjectField } from './../../models/subject-field';
import { DepartmentService } from './../services/department.service';
import { Component, OnInit } from '@angular/core';
import { Semester } from 'src/app/models/semester.enum';

@Component({
  selector: 'app-subject-of-field',
  templateUrl: './subject-of-field.component.html',
  styleUrls: ['./subject-of-field.component.scss']
})
export class SubjectOfFieldComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Liste des matières",
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  subjectFields: Array<SubjectField> = new Array<SubjectField>();

  semestreValue = [
    {
      key: Semester.SEMESTER_1,
      value : "Semestre 1"
    },
    {
      key: Semester.SEMESTER_2,
      value : "Semestre 2"
    },
    {
      key: Semester.SEMESTER_3,
      value : "Semestre 3"
    },
    {
      key: Semester.SEMESTER_4,
      value : "Semestre 4"
    },
    {
      key: Semester.SEMESTER_5,
      value : "Semestre 5"
    },
    {
      key: Semester.SEMESTER_6,
      value : "Semestre 6"
    }
  ]

  constructor(private departmentService : DepartmentService,
    public dialog: MatDialog, private ressourceService: RessourceService) {

  }

  ngOnInit(): void {
    this.fecthSubjectField();
  }


  addSubjectField(){
    const dialogRef = this.dialog.open(AddSubjectFieldComponent, {
      width: "700px",
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: SubjectField) => {
      if (result) {
        this.subjectFields.push(result);
      }
    });
  }

  removeSubjectField(subjectField: SubjectField){

    const alertDialogRef = this.dialog.open(ModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment retirer cette matiere?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(RemoveSubjectFieldComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ subjectFieldId: subjectField.id})),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const index = this.subjectFields.findIndex((elmt) => {
              return (elmt.id == subjectField.id);
            });
            this.subjectFields.splice(index!, 1);
          }
        });
      }
    });
  }

  refactorElement(semester: Semester, subjectField: Array<SubjectField>) : Array<SubjectField>{
    let value  : Array<SubjectField> =  [];
    subjectField.forEach(element => {
      if(element.semester == semester){
        value.push(element);
      }
    });
    return value;
  }

  fecthSubjectField(){
    this.departmentService.getSubjectField()
      .subscribe(arg => this.subjectFields = arg);

  }

  relaod() {
    this.isReloading = true;
    // this.fetchMarket();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }


}
