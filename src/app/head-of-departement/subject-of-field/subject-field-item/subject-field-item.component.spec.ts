import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectFieldItemComponent } from './subject-field-item.component';

describe('SubjectFieldItemComponent', () => {
  let component: SubjectFieldItemComponent;
  let fixture: ComponentFixture<SubjectFieldItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubjectFieldItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectFieldItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
