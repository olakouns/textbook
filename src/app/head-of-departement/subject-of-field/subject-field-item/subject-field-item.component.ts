import { SubjectField } from './../../../models/subject-field';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'src/app/models/subject';

@Component({
  selector: 'app-subject-field-item',
  templateUrl: './subject-field-item.component.html',
  styleUrls: ['./subject-field-item.component.scss']
})
export class SubjectFieldItemComponent implements OnInit {

  @Input() subjectField!: SubjectField;
  @Output() removeSubjectField: EventEmitter<void> = new EventEmitter<void>();
  subject!: Subject;
  constructor() { }

  ngOnInit(): void {
    this.subject = this.subjectField.subject;
  }

}
