import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatBadgeModule } from '@angular/material/badge';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import { HeadOfDepartementRoutingModule } from './head-of-departement-routing.module';
import { HeadCroquisComponent } from './head-croquis/head-croquis.component';
import { DialogCroquisComponent } from './dialog-croquis/dialog-croquis.component';
import { ModalAlertComponent } from './modal-alert/modal-alert.component';
import { SubjectOfFieldComponent } from './subject-of-field/subject-of-field.component';
import { ProgrammCourseComponent } from './programm-course/programm-course.component';
import { MyClassRoomsComponent } from './my-class-rooms/my-class-rooms.component';
import { HeadDashbordComponent } from './head-dashbord/head-dashbord.component';
import { MyClassRoomItemComponent } from './my-class-rooms/my-class-room-item/my-class-room-item.component';
import { MyClassRoomDetailsComponent } from './my-class-rooms/my-class-room-details/my-class-room-details.component';
import { AddSubjectFieldComponent } from './subject-of-field/add-subject-field/add-subject-field.component';
import { SubjectFieldItemComponent } from './subject-of-field/subject-field-item/subject-field-item.component';
import { RemoveSubjectFieldComponent } from './subject-of-field/remove-subject-field/remove-subject-field.component';
import { AddProgrammeComponent } from './my-class-rooms/my-class-room-details/add-programme/add-programme.component';
import { ProgrammItemComponent } from './my-class-rooms/my-class-room-details/programm-item/programm-item.component';
import { RemoveProgrammeComponent } from './my-class-rooms/my-class-room-details/remove-programme/remove-programme.component';
import { EditProgrammeComponent } from './my-class-rooms/my-class-room-details/edit-programme/edit-programme.component';
import { AddProCalendarComponent } from './my-class-rooms/my-class-room-details/add-pro-calendar/add-pro-calendar.component';
import { ShowProcalendarComponent } from './my-class-rooms/my-class-room-details/show-procalendar/show-procalendar.component';
import { ShowSchoolTimeTableComponent } from './my-class-rooms/my-class-room-details/show-school-time-table/show-school-time-table.component';
import { ProfileComponent } from './profile/profile.component';
import { ShowSeancesComponent } from './my-class-rooms/my-class-room-details/show-seances/show-seances.component';
import { AddRepresentativeComponent } from './my-class-rooms/add-representative/add-representative.component';


@NgModule({
  declarations: [
    HeadCroquisComponent,
    DialogCroquisComponent,
    ModalAlertComponent,
    SubjectOfFieldComponent,
    ProgrammCourseComponent,
    MyClassRoomsComponent,
    HeadDashbordComponent,
    MyClassRoomItemComponent,
    MyClassRoomDetailsComponent,
    AddSubjectFieldComponent,
    SubjectFieldItemComponent,
    RemoveSubjectFieldComponent,
    AddProgrammeComponent,
    ProgrammItemComponent,
    RemoveProgrammeComponent,
    EditProgrammeComponent,
    AddProCalendarComponent,
    ShowProcalendarComponent,
    ShowSchoolTimeTableComponent,
    ProfileComponent,
    ShowSeancesComponent,
    AddRepresentativeComponent
  ],
  imports: [
    CommonModule,
    HeadOfDepartementRoutingModule,
    NgxPermissionsModule.forChild(),
    MatCardModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatBadgeModule,
    MatDialogModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientJsonpModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    FormsModule,
    NgxMaterialTimepickerModule
  ],
  exports: [DialogCroquisComponent, ModalAlertComponent],
  providers: [
    MatDatepickerModule,
    NgxMaterialTimepickerModule
  ],
  entryComponents: [
    AddSubjectFieldComponent,
    RemoveSubjectFieldComponent,
    AddProgrammeComponent,
    RemoveProgrammeComponent,
    EditProgrammeComponent,
    AddProCalendarComponent,
    ShowProcalendarComponent,
    ShowSeancesComponent,
    AddRepresentativeComponent
  ]
})
export class HeadOfDepartementModule { }
