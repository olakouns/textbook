import { StatClassRoom } from './../../models/stat-class-room';
import { StatProgram } from './../../models/stat-program';
import { Seance } from './../../models/seance';
import { TimesTables } from './../../models/times-tables';
import { ProCalendar } from './../../models/pro-calendar';
import { ProgrammeCourse } from './../../models/programme-course';
import { ApiResponse } from './../../payload/api-response';
import { Semester } from './../../models/semester.enum';
import { SubjectField } from './../../models/subject-field';
import { ClassRoom } from './../../models/class-room';
import { Page } from './../../payload/page';
import { Subject } from './../../models/subject';
import { Field } from './../../models/field';
import { Constants } from './../../constants';
import { AcademicYear } from './../../models/academic-year';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private httpClient: HttpClient) { }

  getCurrentAcademicYear(): Observable<AcademicYear> {
    return this.httpClient.get<AcademicYear>(Constants.URL + `resources/academic-year/current`,);
  }

  getCurrentField(): Observable<Field>{
    return this.httpClient.get<Field>(Constants.URL + `resources/field/current`,);
  }

  getSubjecs(search: string, page: number, size: number)
  : Observable<Page<Subject>> {
  let params = new HttpParams()
    .set("page", `${page}`)
    .set("search", `${search}`)
    .set("size", `${size}`);

    return this.httpClient.get<Page<Subject>>(Constants.URL + `resources/subjects`, { params: params });
  }

  getClassRoom(fieldId: number): Observable<Array<ClassRoom>> {
    return this.httpClient.get<Array<ClassRoom>>(Constants.URL + `resources/field/${fieldId}/classRoom`,);
  }

  getOneClassRoom(ClassRoomId: number): Observable<ClassRoom> {
    return this.httpClient.get<ClassRoom>(Constants.URL + `resources/field/classRoom/${ClassRoomId}`,);
  }



  addSubjectField(subjectId : string, semester: Semester,  subjectField: SubjectField): Observable<SubjectField>{
    let params = new HttpParams()
    .set("subjectId", `${subjectId}`)
    .set("semester", `${semester}`);
    return this.httpClient.post<SubjectField>(Constants.URL + `department/subjects`, subjectField, { params: params });
  }

  getSubjectField(): Observable<Array<SubjectField>> {
    return this.httpClient.get<Array<SubjectField>>(Constants.URL + `department/subjects`);
  }

  getSeance(subjectFieldId : string, search: string, page: number, size: number): Observable<Page<Seance>>{
    let params = new HttpParams()
    .set("subjectFieldId", `${subjectFieldId}`)
    .set("search", `${search}`)
    .set("page", `${page}`)
    .set("size", `${size}`);
    return this.httpClient.get<Page<Seance>>(Constants.URL + `head-class/seances/by-subject-field`, { params: params });
  }

  deleteSubjectField(subjectFieldId: string): Observable<ApiResponse> {
    return this.httpClient.delete<ApiResponse>(Constants.URL + `department/subjects/${subjectFieldId}`);
  }

  makeProgrammeCourse(profId : string, subjectFieldId : string, classRoomId: number, programmeCourse : ProgrammeCourse): Observable<ProgrammeCourse>{
    let params = new HttpParams()
    .set("profId", `${profId}`)
    .set("subjectFieldId", `${subjectFieldId}`)
    .set("classRoomId", `${classRoomId}`);
    return this.httpClient.post<ProgrammeCourse>(Constants.URL + `department/programming`, programmeCourse, { params: params });
  }

  getProgrammeCourses(classRoomId: number): Observable<Array<ProgrammeCourse>>{
    let params = new HttpParams()
    .set("classRoomId", `${classRoomId}`);
    return this.httpClient.get<Array<ProgrammeCourse>>(Constants.URL + `department/programming`,  { params: params });
  }

  updateProgrammeCourse(programmeCourseId: string, programmeCourse : ProgrammeCourse): Observable<ProgrammeCourse>{
    return this.httpClient.put<ProgrammeCourse>(Constants.URL + `department/programming/${programmeCourseId}`, programmeCourse);
  }

  makeStarted(programmeCourseId: string): Observable<ApiResponse>{
    return this.httpClient.put<ApiResponse>(Constants.URL + `department/programming/${programmeCourseId}/make-started`, "");
  }

  deleteProgramming(programmeCourseId: string): Observable<ApiResponse>{
    return this.httpClient.delete<ApiResponse>(Constants.URL + `department/programming/${programmeCourseId}`);
  }

  makeProCalendar(programmeCourseId: string, proCalendar: ProCalendar): Observable<ProCalendar>{
    return this.httpClient.post<ProCalendar>(Constants.URL + `department/programming/${programmeCourseId}/pro-calendar`, proCalendar);
  }

  getAllProCalendarField(classRoomId: number): Observable<Array<ProCalendar>>{
    let params = new HttpParams()
    .set("classRoomId", `${classRoomId}`);
    return this.httpClient.get<Array<ProCalendar>>(Constants.URL + `department/programming/pro-calendars`, { params: params });
  }

  getProCalendarField(programmeCourseId: string, classRoomId: number): Observable<Array<ProCalendar>>{
    let params = new HttpParams()
    .set("classRoomId", `${classRoomId}`);
    return this.httpClient.get<Array<ProCalendar>>(Constants.URL + `department/programming/${programmeCourseId}/pro-calendar/field`, { params: params });
  }


  getAllTimesTables(): Observable<Array<TimesTables>>{
    return this.httpClient.get<Array<TimesTables>>(Constants.URL +  `department/programming/pro-calendar/all-class-room`);
  }


  getTimesDone(programmeCourseId: string): Observable<StatProgram>{
    return this.httpClient.get<StatProgram>(Constants.URL +  `department/programming/${programmeCourseId}/stat-program`);
  }

  getStatClassRoom(classRoomId: number): Observable<StatClassRoom>{
    return this.httpClient.get<StatClassRoom>(Constants.URL +  `department/programming/${classRoomId}/stat-class-room`);
  }

  getStatClassRoomGeneral(): Observable<StatClassRoom>{
    return this.httpClient.get<StatClassRoom>(Constants.URL +  `department/programming/stat-class-room/all`);
  }


}
