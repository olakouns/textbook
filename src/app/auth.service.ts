import { Actor } from './models/actor';
import { DecodeToken } from './payload/decode-token';
import { NgxPermissionsService } from 'ngx-permissions';
import { LoginRequest } from './payload/login-request';
import { Constants } from './constants';
import { Token } from './payload/token';
import { User } from './models/user';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {map} from "rxjs/operators";
import { Router } from '@angular/router';
import  jwt_decode from 'jwt-decode';
// import * as jwt_decode from 'jwt-decode';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // private currentUserSubject: BehaviorSubject<User>;
  // public currentUser: Observable<User>;

  // private tokenSubject: BehaviorSubject<Token>;
  // public token: Observable<Token>;

  constructor(private http: HttpClient,
    private router: Router,
    private permissionsService: NgxPermissionsService,) {

    // this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser') ));
    // this.currentUser = this.currentUserSubject.asObservable();

    // this.tokenSubject = new BehaviorSubject<Token>(JSON.parse(localStorage.getItem('token')));
    // this.token = this.tokenSubject.asObservable();
  }

  // public get currentUserValue(): User {
  //   return this.currentUserSubject.value;
  // }

  public get currentTokenValue(): Token {
    return new BehaviorSubject<Token>(JSON.parse(localStorage.getItem('token') || "")).value;
  }

  login(loginRequest: LoginRequest) {
    return this.http.post<any>(Constants.URL + `auth/login`, loginRequest)
      .pipe(map(token => {
        if((<Token>token).token){
          console.log(token);
          localStorage.setItem('token', JSON.stringify(token));
          // this.tokenSubject.next(token);
          const value = token.token;
          // option: JwtDecodeOptions
          const decodedToken = jwt_decode<DecodeToken>(value);
          console.log(decodedToken.roles);
          this.permissionsService.loadPermissions(decodedToken.roles);

        }
        else{
        }
        return token;
    }));
  }

  getMe(): Observable<Actor> {
    return this.http.get<Actor>(Constants.URL + `actors/me`);
  }


  isAuth() {
    if (localStorage.getItem("token") == null) {
      return false;
    }
    let value  = localStorage.getItem("token")!;
    const token: Token = JSON.parse(value);
    // console.log("token", token);
    if (token.token == null) {
      return false;
    }
    return true;

  }

  getAccessToken() {
    if (!this.isAuth()) {
      return null;
    }
    // const token: Token = this.decryptData(localStorage.getItem("token"));
    const token: Token = JSON.parse(localStorage.getItem("token")!);
      return token.token;

  }

  logout(){
    if (localStorage.getItem("token") != null) {
      localStorage.removeItem("token");
      // this.profile = null;
    }
    // clearTimeout(this.timerKeep);
    this.router.navigateByUrl("/");
  }

  getImageURL(url: string) {
    if (url != null && url.length !== 0) {
      return Constants.URL_MEDIA + url;
    } else {
      return "assets/images/avatar.svg";
    }
  }


}
