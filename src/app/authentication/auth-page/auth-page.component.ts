import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit {

  loadPreviews: boolean = false;
  isLogin: boolean = true;
  isForgot: boolean = false;
  changeDefaultPassword: boolean = false;
  changePassword: boolean = false;
  signup: boolean = false;

  constructor(route: ActivatedRoute) {
    route.data.subscribe(res => {
      if (res.type == 'change') {
        this.isLogin = false;
        this.changePassword = true;
      }
      if (res.type == 'signup') {
        this.isLogin = false;
        this.signup = true;
      }
    })
  }


  ngOnInit(): void {
  }

}
