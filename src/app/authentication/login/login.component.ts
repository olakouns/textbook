import { LoginRequest } from './../../payload/login-request';
import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  EventEmitter,
  Output,
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { AuthService } from "src/app/auth.service";
import { NgxPermissionsService } from "ngx-permissions";
import { ActivatedRoute, NavigationExtras, Router } from "@angular/router";
import { isPlatformBrowser } from "@angular/common";
import { Token } from "../../payload/token";
import { ApiResponse } from "src/app/payload/api-response";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output() goToPasswordForgot: EventEmitter<void> = new EventEmitter();
  @Output() goToChangeDefaultPassword: EventEmitter<String> = new EventEmitter();

  form = this.formBuilder.group({
    username: ["", [Validators.required, Validators.email]],
    //password: ['', [Validators.required, Validators.minLength(8)]],
    password: ["", [Validators.required]],
    keepConnect: [false],
  });

  user = {
    username: "",
    password: "",
  };
  errorLogin = false;
  errorMessage = "";
  successMessage = "";
  isLoading = false;
  success = false;
  hide = true;
  // isBrowser: boolean;
  // loginRequest : LoginRequest;
  error = '';
  keepConnect: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private permissionsService: NgxPermissionsService,
    private snackBar: MatSnackBar,
    public router: Router,
    private route: ActivatedRoute,
    // @Inject(PLATFORM_ID) platformId
  ) {
    // this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit() {
    // this.buildForm();
  }

  // buildForm() {
  //   this.form = this.formBuilder.group({
  //     username: ["", [Validators.required, Validators.email]],
  //     //password: ['', [Validators.required, Validators.minLength(8)]],
  //     password: ["", [Validators.required]],
  //     keepConnect: [false],
  //   });
  // }

  get f() {
    return this.form.controls;
  }
  get email() {
    return this.form.get("email");
  }
  get password() {
    return this.form.get("password");
  }

  onSubmit() {
    if (this.form.invalid) {
      let config = new MatSnackBarConfig();
      config.panelClass = ["alert", "bg-danger", "text-white"];
      config.duration = 10000;
      this.snackBar.open("Tous les champs sont obligatoires!", "OK", config);
      return;
    }
    this.isLoading = true;
    this.errorLogin = false;
    // localStorage.setItem("keepConnect", this.keepConnect ? "yes" : "no");

     var loginRequest = new LoginRequest(this.user.username, this.user.password);

    this.authService.login(loginRequest).pipe(first()).subscribe({
        next: () => {
            // get return url from route parameters or default to '/'
            const returnUrl = this.route.snapshot.queryParams['returnUrl'];
            const navigationExtras: NavigationExtras = {
              queryParamsHandling: 'preserve',
              preserveFragment: true
            };

            this.success = true;
            this.permissionsService.hasPermission("ACCESS_HEAD_DEPARTMENT").then(
              (v)=> {
                if(v) {
                  if (returnUrl != null) {
                    this.router.navigate([returnUrl], navigationExtras);
                  } else {
                    this.router.navigateByUrl('head-of-department');
                  }

                } else {
                  this.permissionsService.hasPermission("ACCESS_HEAD_CLASS").then(
                   (value)=>{
                     if (value) {
                      if (returnUrl != null) {
                        this.router.navigate([returnUrl], navigationExtras);
                      } else {
                        this.router.navigateByUrl('head-of-class');
                      }
                     } else {
                      this.permissionsService.hasPermission("ACCESS_TEACHER").then(
                        (val)=>{
                          if (val) {
                            if (returnUrl != null) {
                              this.router.navigate([returnUrl], navigationExtras);
                            } else {
                              this.router.navigateByUrl('teacher');
                            }
                          }else {
                            if (returnUrl != null) {
                              this.router.navigate([returnUrl], navigationExtras);
                            } else {
                              this.router.navigateByUrl('et-admin');
                            }
                          }
                        }
                      ).catch(error => {

                      })
                     }
                   }
                  ).catch(error => {

                  })

                }
              }
            ).catch(err => {

            })
        },
        error: error => {
          // TODO : check if error is 401 an then rshow error
          this.error = error;
          this.isLoading = false;
          console.log(this.error);
        }
    });

  }

}
