import { User } from './../../models/user';
import { AddActor } from './../../payload/add-actor';
import { ApiResponse } from './../../payload/api-response';
import { Actor } from './../../models/actor';
import { Page } from './../../payload/page';
import { Observable } from 'rxjs';
import { Constants } from './../../constants';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ActorService {

  constructor(private httpClient: HttpClient) { }

  getAllTeacher(search: String, page: number, size: number): Observable<Page<Actor>> {
    let params = new HttpParams()
      .set("search", `${search}`)
      .set("page", `${page}`)
      .set("size", `${size}`);

    return this.httpClient.get<Page<Actor>>(
      Constants.URL + `actors/teachers`, { params: params });
  }

  getUserOfActor(actorId: string): Observable<User> {
    return this.httpClient.get<User>(Constants.URL + `actors/${actorId}/user`);
  }

  getMe(): Observable<Actor> {
    return this.httpClient.get<Actor>(Constants.URL + `actors/me`);
  }



  addTeacher(actor: AddActor): Observable<Actor> {
    return this.httpClient.post<Actor>(Constants.URL + `actors/teachers`, actor);
  }

  updateTeacher(actorId: string, actor: AddActor): Observable<Actor> {
    return this.httpClient.put<Actor>(Constants.URL + `actors/teachers/${actorId}`, actor);
  }

  definedActorAsHead(actorId: string, fieldId: number): Observable<Actor> {
    let params = new HttpParams()
    .set("fieldId", `${fieldId}`);
    return this.httpClient.put<Actor>(Constants.URL + `actors/teachers/${actorId}/head`, "" ,{params: params});
  }

  deleteTeacher(actorId: string): Observable<ApiResponse> {
    return this.httpClient.delete<ApiResponse>(Constants.URL + `actors/teachers/${actorId}`);
  }

  addClassRepresentative(actor: AddActor): Observable<Actor> {
    return this.httpClient.post<Actor>(Constants.URL + `actors/class-representative`, actor);
  }


  getImageURL(url: string) {
    if (url != null && url.length !== 0) {
      return Constants.URL_MEDIA + url;
    } else {
      return "assets/images/avatar.svg";
    }
  }

}
