import { StatByField } from './../../models/stat-by-field';
import { StatFirst } from './../../models/stat-first';
import { ClassRoom } from './../../models/class-room';
import { AcademicYear } from './../../models/academic-year';
import { Media } from './../../models/media';
import { Option } from './../../models/option';
import { ApiResponse } from './../../payload/api-response';
import { Field } from './../../models/field';
import { Page } from './../../payload/page';
import { Constants } from './../../constants';
import { Observable } from 'rxjs';
import { Subject } from './../../models/subject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RessourceService {

  constructor(private httpClient: HttpClient) { }

  getCurrentAcademicYear(): Observable<AcademicYear> {
    return this.httpClient.get<AcademicYear>(Constants.URL + `resources/academic-year/current`,);
  }

  createAcademicYear(academicYear: AcademicYear): Observable<AcademicYear> {
    return this.httpClient.post<AcademicYear>(Constants.URL + `resources/academic-year`, academicYear);
  }

  getSubjecs(search: string, page: number, size: number)
  : Observable<Page<Subject>> {
  let params = new HttpParams()
    .set("page", `${page}`)
    .set("search", `${search}`)
    .set("size", `${size}`);

  return this.httpClient.get<Page<Subject>>(Constants.URL + `resources/subjects`, { params: params });
  }

  createSubject(subject: Subject): Observable<Subject> {
    return this.httpClient.post<Subject>(Constants.URL + `resources/subjects`, subject);
  }

  updateSubject(subject: Subject): Observable<Subject> {
    return this.httpClient.put<Subject>(
      Constants.URL + `resources/subjects/${subject.id}`, subject);
  }

  deleteSubject(subjectId: string): Observable<ApiResponse> {
    return this.httpClient.delete<ApiResponse>(Constants.URL + `resources/subjects/${subjectId}`);
  }

// Requetes pour les filieres.
  getField(search: string): Observable<Array<Field>> {
    let params = new HttpParams()
    .set("search", `${search}`);
    return this.httpClient.get<Array<Field>>(Constants.URL + `resources/fields`,  { params: params });
  }

  getFieldsNotHaveHead(): Observable<Array<Field>> {
    return this.httpClient.get<Array<Field>>(Constants.URL + `resources/fields/have-not-head`,);
  }

  createField(field: Field): Observable<Field> {
    return this.httpClient.post<Field>(Constants.URL + `resources/fields`, field);
  }

  updateField(field: Field): Observable<Field> {
    return this.httpClient.put<Field>(Constants.URL + `resources/fields/${field.id}`, field);
  }

  deleteField(fieldId: number): Observable<ApiResponse> {
    return this.httpClient.delete<ApiResponse>(Constants.URL + `resources/fields/${fieldId}`);
  }

  uploadMedia(file: File, type: string, description: string) {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('type', type);
    formdata.append('description', description);
    return this.httpClient.post<Media>(Constants.URL + 'medias', formdata);
  }

  createOption(fieldId: number, option: Option): Observable<Option> {
    return this.httpClient.post<Option>(Constants.URL + `resources/fields/${fieldId}/options`, option);
  }

  updateOption(fieldId: number, optionId: number, option: Option): Observable<Option> {
    return this.httpClient.put<Option>(Constants.URL + `resources/fields/${fieldId}/options/${optionId}`, option);
  }

  deleteOption(fieldId: number, optionId: number): Observable<ApiResponse> {
    return this.httpClient.delete<ApiResponse>(Constants.URL + `resources/fields/${fieldId}/options/${optionId}`);
  }

  createClassRoom(fieldId: number, optionId: number, classRoom: ClassRoom): Observable<ClassRoom> {
    if(optionId == null) optionId = 0;
    // .set("optionId", `${optionId}`); ahounsejeanlouis@gmail.com

    return this.httpClient.post<ClassRoom>(Constants.URL + `resources/classRoom?fieldId=${fieldId}&optionId=${optionId}`, classRoom);
  }

  updateClassRoom(classRoomId: number, fieldId: number, optionId: number, classRoom: ClassRoom): Observable<ClassRoom> {
    if(optionId == null) optionId = 0;

    return this.httpClient.put<ClassRoom>(Constants.URL + `resources/classRoom/${classRoomId}?fieldId=${fieldId}&optionId=${optionId}`,
      classRoom);
  }

  deleteClassRoom(classRoomId: number): Observable<ApiResponse> {
    return this.httpClient.delete<ApiResponse>(Constants.URL + `resources/classRoom/${classRoomId}`);
  }

  addClassRepresentative(classRoomId: number, actorId: string): Observable<ApiResponse> {
    let params = new HttpParams()
    .set("actorId", `${actorId}`);
    return this.httpClient.put<ApiResponse>(Constants.URL + `resources/classRoom/${classRoomId}/class-representative`, "",{ params: params });
  }

  getClassRoom(fieldId: number): Observable<Array<ClassRoom>> {
    return this.httpClient.get<Array<ClassRoom>>(Constants.URL + `resources/field/${fieldId}/classRoom/`,);
  }

  getStatFirst(): Observable<StatFirst> {
    return this.httpClient.get<StatFirst>(Constants.URL + `resources/stat`,);
  }

  getAllStatByField(): Observable<Array<StatByField>> {
    return this.httpClient.get<Array<StatByField>>(Constants.URL + `resources/stat/field`,);
  }

}
