import { TypeRole } from './../../../models/type-role';
import { ActorService } from './../../services/actor.service';
import { Actor } from './../../../models/actor';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-teacher-item',
  templateUrl: './teacher-item.component.html',
  styleUrls: ['./teacher-item.component.scss']
})
export class TeacherItemComponent implements OnInit {
  @Input() teacher: Actor;
  @Output() definedAsHead: EventEmitter<void> = new EventEmitter<void>();
  @Output() removeAsHead: EventEmitter<void> = new EventEmitter<void>();
  @Output() editTeacher: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteTeacher: EventEmitter<void> = new EventEmitter<void>();

  title = [
    {
      key: TypeRole.TEACHER,
      value: "Enseignant"
    },
    {
      key: TypeRole.HEAD_OF_DEPARTMENT,
      value: "Chef département"
    }
  ]
  constructor(public actorService: ActorService) { }

  ngOnInit(): void {
  }

  checkTitle(){
    return (this.teacher.type == TypeRole.HEAD_OF_DEPARTMENT) ? "Chef département" : "Enseignant";
  }

}
