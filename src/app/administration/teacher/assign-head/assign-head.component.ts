import { Actor } from './../../../models/actor';
import { Field } from './../../../models/field';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { ActorService } from './../../services/actor.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-assign-head',
  templateUrl: './assign-head.component.html',
  styleUrls: ['./assign-head.component.scss']
})
export class AssignHeadComponent implements OnInit {
  title: String = "Definir l'enseignant comme chef departement";
  validateText: String = "Valider";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode: String;
  error: boolean = false;
  errorMessage: string = "";
  fieldIdSelected: number;
  fields: Array<Field> = [];

  constructor(private formBuilder: FormBuilder,
    private actorService : ActorService,
    private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<AssignHeadComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.fetchNotHeadField();
    this.form = this.formBuilder.group({
      field: ['', Validators.required]
    });

  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    this.actorService.definedActorAsHead(this.data.teacherId, this.fieldIdSelected).subscribe(
      (res: Actor) => {
        this.dialogRef.close(res);
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        this.error = true;
        this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
      });
  }

  fetchNotHeadField(){
    this.ressourceService.getFieldsNotHaveHead().subscribe(
      (res)=>{
        this.fields = res;
      }
    )
  }

}
