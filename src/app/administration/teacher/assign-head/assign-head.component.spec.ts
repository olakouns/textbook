import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignHeadComponent } from './assign-head.component';

describe('AssignHeadComponent', () => {
  let component: AssignHeadComponent;
  let fixture: ComponentFixture<AssignHeadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignHeadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
