import { RemoveAssigningComponent } from './remove-assigning/remove-assigning.component';
import { AssignHeadComponent } from './assign-head/assign-head.component';
import { EtModalAlertComponent } from './../et-modal-alert/et-modal-alert.component';
import { AddTeacherComponent } from './add-teacher/add-teacher.component';
import { AddActor } from './../../payload/add-actor';
import { ActorService } from './../services/actor.service';
import { Actor } from './../../models/actor';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { RessourceService } from './../services/ressource.service';
import { Subscription } from 'rxjs';
import { Page } from './../../payload/page';
import { Component, OnInit } from '@angular/core';
import { DeleteTeacherComponent } from './delete-teacher/delete-teacher.component';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss']
})
export class TeacherComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Liste des enseignants",
      // link: "/settings"
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  page: number = 0;
  size: number = 20;
  pageTeacher: Page<Actor> = new Page<Actor>();
  teachers: Array<Actor> = new Array<Actor>();
  error: boolean = false;
  messageError = "";
  teacheerSubscription!: Subscription;
  search = "";
  form = this.formBuilder.group({
    search: ["", [Validators.required]],
  });



  constructor(private formBuilder: FormBuilder,
    public dialog: MatDialog, private ressourceService: RessourceService,
    private actorService: ActorService) { }

  ngOnInit(): void {
    this.fectTeacher();
  }

  createTeacher(){
    const dialogRef = this.dialog.open(AddTeacherComponent, {
      width: "700px",
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Actor) => {
      if (result) {
        this.teachers.push(result);
      }
    });
  }

  editTeacher(teacher: Actor){
    const dialogRef = this.dialog.open(AddTeacherComponent, {
      width: "700px",
      data: teacher,
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Actor) => {
      if (result) {
        const index = this.teachers.findIndex((elmt) => {
          return (elmt.id == teacher.id);
        });
        if (index != -1) {
          this.teachers[index] = result;
        }
      }
    });
  }

  deleteTeacher(teacher: Actor){
    const alertDialogRef = this.dialog.open(EtModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment retirer l\'enseignant?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(DeleteTeacherComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ teacherId: teacher.id})),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const index = this.teachers.findIndex((elmt) => {
              return (elmt.id == teacher.id);
            });
            this.teachers.splice(index!, 1);
          }
        });
      }
    });
  }

  definedAsHead(teacherId: string){
    const dialogRef = this.dialog.open(AssignHeadComponent, {
      width: "700px",
      data: { teacherId: teacherId  },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Actor) => {
      if (result) {
        const index = this.teachers.findIndex((elmt) => {
          return (elmt.id == teacherId);
        });
        if (index != -1) {
          this.teachers[index] = result;
        }
      }
    });
  }

  removeAsHead(teacherId: string){
    const alertDialogRef = this.dialog.open(EtModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous retirer le titre?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(RemoveAssigningComponent, {
          width: "700px",
          data: { teacherId: teacherId  },
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result: Actor) => {
          if (result) {
            const index = this.teachers.findIndex((elmt) => {
              return (elmt.id == teacherId);
            });
            if (index != -1) {
              this.teachers[index] = result;
            }
          }
        });

      }
    });

  }

  setPage(event : any) {
    if (event.pageSize != this.size) {
      this.size = event.pageSize;
      this.page = 0;
    } else {
      this.page = event.pageIndex;
    }
    this.fectTeacher();
  }

  fectTeacher(){
    this.isLoading = true;
    this.actorService.getAllTeacher(this.search, this.page, this.size).subscribe(
      (response)=>{
        this.pageTeacher = response;
        this.teachers = response.content;
        this.isLoading = false;
        this.isTimeout = false;
      },
      (error)=>{
        this.error = true;
        this.isLoading = false;
        this.catchError(error);
      }
    );
  }


  filterActor(event: any) {


  }


  relaod() {
    this.isReloading = true;
    // this.fetchMarket();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
