import { Media } from './../../../models/media';
import { RessourceService } from './../../services/ressource.service';
import { User } from './../../../models/user';
import { AddActor } from './../../../payload/add-actor';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditSubjectComponent } from './../../subject/edit-subject/edit-subject.component';
import { ActorService } from './../../services/actor.service';
import { Actor } from './../../../models/actor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.scss']
})
export class AddTeacherComponent implements OnInit {
  title: String = "Ajouter un enseignant";
  validateText: String = "Valider";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode: String;
  error: boolean = false;
  errorMessage: string = "";
  teacher: Actor;
  addActor: AddActor = new AddActor();
  user: User = new User();
  show = false;

  fileChanged: boolean = false;
  fileBtnText = "Choisir";
  file: File;
  loadFileText = "Definir une image de profil";

  constructor(private formBuilder: FormBuilder,
    private actorService : ActorService,
    private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<EditSubjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Actor) {

      if (this.data == null) {
        this.show = true;
        this.mode = "new";
        this.teacher = new Actor();
        this.addActor = new AddActor();
      } else {
        this.mode = "edit";
        this.title  = "Modifier les informations d'un enseignats";
        this.teacher = Object.assign({}, this.data);
        this.getUserofActor();
      }
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      email: ['', Validators.required],
      title: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      password: this.mode == "edit" ? [''] : ['', Validators.required],
      file: this.mode == "edit" ? [''] : ['', Validators.required],
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    if (this.mode === "new") {
      this.ressourceService.uploadMedia(this.file, "images", this.addActor.firstName)
      .subscribe(
        (res: Media) => {
          this.addActor.media = res;

          this.actorService.addTeacher(this.addActor)
            .subscribe(
              (res: Actor) => {
                this.dialogRef.close(res);
                this.isLoading = false;
              },
              (err) => {
                this.isLoading = false;
                this.error = true;
                this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
              });
        });

    } else if (this.mode === "edit") {
      if (this.fileChanged) {
        this.ressourceService.uploadMedia(this.file, "images", this.addActor.firstName)
          .subscribe(
            (res: Media) => {
              this.addActor.media = res;
              this.actorService.updateTeacher(this.teacher.id, this.addActor)
              .subscribe((res) => {
                  this.dialogRef.close(res);
                  this.isLoading = false;
                });
            });

      } else {
        this.actorService
        .updateTeacher(this.teacher.id, this.addActor).subscribe((res) => {
            this.dialogRef.close(res);
            this.isLoading = false;
          });
      }

    }
  }


  getUserofActor(){
    this.actorService.getUserOfActor(this.teacher.id).subscribe(
      (response)=>{
        this.user = response;
        this.addActor.firstName = this.teacher.firstName;
        this.addActor.lastName = this.teacher.lastName;
        this.addActor.address = this.teacher.address;
        this.addActor.phone = this.teacher.phone;
        this.addActor.title = this.teacher.title;
        this.addActor.email = response.email;
        this.addActor.password = response.password;
        this.addActor.media = this.teacher.mediaFile;
        this.show = true;

        this.loadFileText = this.data.mediaFile.originalName;
        this.fileBtnText = "Changer";
      },
      (error)=>{
        this.error = true;
        this.isLoading = false;
        this.catchError(error);
      }
    )
  }

  loadFileChoosed(selectedFiles: any) {
    if (selectedFiles == null || selectedFiles.length < 1) {
      return;
    }
    this.file = selectedFiles[0];
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(this.file);
    const extension = this.file.name.substring(this.file.name.lastIndexOf('.'), this.file.name.length);
    this.loadFileText = this.file.name.length <= 40 ? this.file.name : this.file.name.substring(0, 35) + '..' + extension;
    if (this.mode === "edit") {
      this.fileChanged = true;
    }
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
    } else {
      this.isTimeout = false;
    }
  }

}
