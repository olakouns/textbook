import { Actor } from './../../../models/actor';
import { ActorService } from './../../services/actor.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-delete-teacher',
  templateUrl: './delete-teacher.component.html',
  styleUrls: ['./delete-teacher.component.scss']
})
export class DeleteTeacherComponent implements OnInit {
  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private ressourceService: RessourceService,
    private actorService: ActorService,
    public dialogRef: MatDialogRef<DeleteTeacherComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.removeTeacher();
  }

  removeTeacher(){
    this.actorService
    .deleteTeacher(this.data.teacherId)
    .subscribe(
      (response: any) => {
        this.dialogRef.close(response);
      },
      (error) => {
        this.dialogRef.close();
      }
    );
  }

}
