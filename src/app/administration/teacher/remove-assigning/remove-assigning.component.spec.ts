import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveAssigningComponent } from './remove-assigning.component';

describe('RemoveAssigningComponent', () => {
  let component: RemoveAssigningComponent;
  let fixture: ComponentFixture<RemoveAssigningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveAssigningComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveAssigningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
