import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { ActorService } from './../../services/actor.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-remove-assigning',
  templateUrl: './remove-assigning.component.html',
  styleUrls: ['./remove-assigning.component.scss']
})
export class RemoveAssigningComponent implements OnInit {

  constructor( private actorService : ActorService,
    private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<RemoveAssigningComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

}
