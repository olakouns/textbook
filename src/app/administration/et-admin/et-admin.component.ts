import { Actor } from './../../models/actor';
import { RessourceService } from './../services/ressource.service';
import { isPlatformBrowser } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxPermissionsService } from 'ngx-permissions';
import { AuthService } from 'src/app/auth.service';
import { Router } from '@angular/router';
import { ChangeDetectorRef,
  Component,
  EventEmitter, HostListener,
  Inject,
  Input,
  OnInit,
  Output,
  PLATFORM_ID,
  ViewChild } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { BreakpointObserver, Breakpoints, MediaMatcher } from '@angular/cdk/layout';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { MatToolbar } from '@angular/material/toolbar';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-et-admin',
  templateUrl: './et-admin.component.html',
  styleUrls: ['./et-admin.component.scss']
})
export class EtAdminComponent implements OnInit {

  constructor(
    private ressourceService: RessourceService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    public router: Router,
    public authService: AuthService,
    breakpointObserver: BreakpointObserver,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private permissionsService: NgxPermissionsService,
    /*private notificationService: NotificationService,*/
    @Inject(PLATFORM_ID) platformId: any,
    private snackBar: MatSnackBar
  ) {
    this.isBrowser = isPlatformBrowser(platformId);

    iconRegistry.addSvgIcon(
      'dashboard',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/dashboard.svg'
      )
    );

    iconRegistry.addSvgIcon(
      'services',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/services.svg'
      )
    );

    iconRegistry.addSvgIcon(
      'patient',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/patient.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'Medom Coprs',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/Medom Coprs.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'blogger',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/blogger.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'post_1',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/post_1.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'category_post',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/category_post.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'pharmacy-icon',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/pharmacy-icon.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'pill',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/pill.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'transactions',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/transactions.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'parametre',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/parametre.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'role_manager',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/role_manager.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'user_manager',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/user_manager.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'filière',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/filière.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'matière',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/matière.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'teacher',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/teacher.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'settings',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/settings.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'classes',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/class.svg'
      )
    );


    breakpointObserver
      .observe([
        Breakpoints.Handset,
        // Breakpoints.Tablet,
        // Breakpoints.Web,
      ])
      .subscribe((result) => {
        if (result.matches) {
          // this.activateHandsetLayout();
          this.mode = 'over';
          this.hasBackdrop = false;
          this.options.fixed = true;
        } else {
          this.mode = 'side';
          this.hasBackdrop = true;
          this.options.fixed = false;
        }
      });

    this.mobileQuery = media.matchMedia('(max-width: 767px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  // tslint:disable-next-line:typedef
  // get typeNotification() {
  //   return TypeNotification;
  // }

  @Input() title!: string;
  @Input() linkTree: Array<any> = [];
  @Input() isTimeout = false;
  @Input() isReloading = false;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onReload: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('sidenav', { static: true }) sidenav!: MatSidenav;
  @ViewChild('toolbar', { static: true }) toolbar!: MatToolbar;
  @ViewChild('notificationMenu', { static: true }) trigger!: MatMenuTrigger;
  options = {
    bottom: 0,
    fixed: false,
    top: 0,
  };
  toolbarFullWidth = false;
  links = [
    {
      name: 'Tableau de bord',
      link: '/et-admin/dashbord',
      icon: 'dashboard',
      permissions: [],
    },
    {
      name: 'Enseignants',
      link: '/et-admin/actors',
      icon: 'teacher',
      permissions: [],
    },
    {
      name: 'Matières',
      link: '/et-admin/subject',
      icon: 'matière',
      permissions: [],
    },
    {
      name: 'Filières',
      link: '/et-admin/field',
      icon: 'filière',
      submenus: [],
      permissions: [],
    },
    // {
    //   name: 'Medom Corps',
    //   link: '/medom/agents',
    //   icon: 'Medom Coprs',
    //   permissions: [],
    // },
    // {
    //   name: 'Pharmacies',
    //   link: '/medom/pharmacies',
    //   icon: 'pharmacy-icon',
    //   permissions: [],
    // },
    // {
    //   name: 'Produits Pharmaceutiques',
    //   link: '/medom/pharmaceuticals',
    //   icon: 'pill',
    //   permissions: [],
    // },
    // {
    //   name: 'Transactions',
    //   link: '/medom/transactions',
    //   icon: 'transactions',
    //   permissions: [],
    // },
    // {
    //   name: 'Blog',
    //   link: '/medom/blog',
    //   icon: 'blogger',
    //   submenus: [
    //     {
    //       name: 'Gestion des Postes',
    //       link: '/medom/blog/posts',
    //       icon: 'post_1',
    //       permissions: [],
    //     },
    //     {
    //       name: 'Catégories de Poste',
    //       link: '/medom/blog/post-categories',
    //       icon: 'category_post',
    //       permissions: [],
    //     },
    //   ],
    //   permissions: [],
    // },
    // {
    //   name: 'Paramètre',
    //   link: '/medom/settings',
    //   icon: 'settings',
    //   submenus: [
    //     {
    //       name: 'Gestion des Roles',
    //       link: '/medom/settings/roles',
    //       icon: 'role_manager',
    //       permissions: [],
    //     },
    //     {
    //       name: 'Gestion des Utilisateurs',
    //       link: '/medom/settings/users',
    //       icon: 'user_manager',
    //       permissions: [],
    //     },
    //   ],
    //   permissions: [],
    // },
  ];
  isBrowser: boolean;
  isLoadingProfile = false;
  isLoadingNotifications = true;
  // profile: Profile;
  page = 0;
  size = 20;
  search = '';
  nbNotification = 0;
  // selectedStatus: TypeNotification;
  // pageNotification: Page<Notification> = new Page<Notification>();
  // notifications: Array<Notification> = new Array<Notification>();

  hasBackdrop = false;
  mode = 'side' as MatDrawerMode; // over

  mobileQuery: MediaQueryList;
  actor : Actor;

  // tslint:disable-next-line:variable-name
  private readonly _mobileQueryListener: () => void;

  // tslint:disable-next-line:typedef
  ngOnInit() {
    if (!this.isBrowser) { return; }
    // this.resizeChart(window.innerWidth);
    this.sidenav.openedChange.subscribe((res) => {
      this.toolbarFullWidth = !res;
    });
    this.isLoadingProfile = true;
    this.fecthCurrentYear();
    // this.authService.getMe().subscribe((profile) => {
    //   this.profile = profile;
    //   this.isLoadingProfile = false;
    //   this.permissionsService.addPermission(this.authService.getPermissions());

    // });
    // this.authService.getNotificationSize().subscribe((count) => {
    //   this.nbNotification = count;
    // });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }



  fecthCurrentYear(){
    this.ressourceService.getCurrentAcademicYear().subscribe(
      (response)=>{
        this.links.splice(1, 0,  {
          name: 'Classes',
          link: '/et-admin/class-room',
          icon: 'classes',
          permissions: [],
        })
      },
      (error)=>{
        // if (error.status) {
        //   this.currentAcademicYear = null;
        // }

      }
    );
  }

  getMe(){
    this.authService.getMe().subscribe(
      (response)=>{
        this.actor = response;
      }
    );
  }

  deconnexion(): any {
    this.authService.logout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event : any): any {
    // console.log(event.target.innerWidth);
  }

  // viewNotification(notification: Notification): any {
  //   // todo toute la fonction est a redefinir
  //   let url = `ac-admin/`;
  //   switch (notification.type) {
  //     case this.typeNotification.NEW_PROJECT:
  //       url += `projects/${notification.objectId}/details`;
  //       break;

  //     case this.typeNotification.RESPONSE_TO_ASK_DOCUMENT_PROJECT:
  //       url += `projects/${notification.objectId}/details`;
  //       break;

  //     case this.typeNotification.ALERT_SUBMISSION_RAPPORT_PROJECT:
  //       url += `projects/${notification.objectId}/details`;
  //       break;

  //     case this.typeNotification.ASK_VALIDATE_DOCUMENT_PROJECT:
  //       url += `projects/${notification.objectId}/details`;
  //       break;

  //     default:
  //       break;
  //   }
  //   this.router.navigateByUrl(url);
  // }
}
