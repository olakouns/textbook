import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtAdminComponent } from './et-admin.component';

describe('EtAdminComponent', () => {
  let component: EtAdminComponent;
  let fixture: ComponentFixture<EtAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
