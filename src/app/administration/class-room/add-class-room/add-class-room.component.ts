import { Field } from './../../../models/field';
import { Years } from './../../../models/years.enum';
import { ClassRoom } from './../../../models/class-room';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-add-class-room',
  templateUrl: './add-class-room.component.html',
  styleUrls: ['./add-class-room.component.scss']
})
export class AddClassRoomComponent implements OnInit {

  title: String = "Ajouter une classe";
  validateText: String = "Ajouter";
  form: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode!: String;
  error: boolean = false;
  errorMessage: string = "";
  classRoom: ClassRoom;
  selectedOption: any;
  field: Field;
  selectOptionId: number;

  yearsValue= [
    {
      key: Years.THIRST_YEAR,
      value: "Première année"
    },
    {
      key: Years.SECOND_YEAR,
      value: "Deuxième année"
    },
    {
      key: Years.THIRD_YEAR,
      value: "Troisième année"
    }
  ]

  constructor(private formBuilder: FormBuilder,
    private ressourceService : RessourceService,
    public dialogRef: MatDialogRef<AddClassRoomComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.field = this.data.field;
      if (this.data.classRoom == null) {
        this.mode = "new";
        this.classRoom = new ClassRoom();
      } else {
        this.mode = "edit";
        this.title  = "Modifier une classe";
        this.classRoom = Object.assign({}, this.data.classRoom);
        this.selectOptionId = this.classRoom?.option?.id;
      }
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      year: ['', Validators.required],
      option: ['']
    });
  }

  getOption(){
    const index = this.field?.options!.findIndex((elmt) => {
      return (elmt.id == this.selectOptionId);
    });
    if (index != -1) {
      this.classRoom.option = this.field?.options![index];
    }
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    this.getOption();
    if (this.mode === "new") {
      this.ressourceService.createClassRoom(this.field.id, this.selectOptionId, this.classRoom).subscribe(
        (response)=>{
          this.dialogRef.close(response);
          this.isLoading = false;
        }
      );
    }else{

      this.ressourceService.updateClassRoom(this.classRoom.id, this.field.id, this.selectOptionId, this.classRoom).subscribe(
        (response)=>{
          this.dialogRef.close(response);
          this.isLoading = false;
        }
      )
    }



  }

}
