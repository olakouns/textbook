import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-delete-class-room',
  templateUrl: './delete-class-room.component.html',
  styleUrls: ['./delete-class-room.component.scss']
})
export class DeleteClassRoomComponent implements OnInit {
  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<DeleteClassRoomComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.deleteClassRoom();
  }


  deleteClassRoom() {
    this.ressourceService
      .deleteClassRoom(this.data.classRoomId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }
}
