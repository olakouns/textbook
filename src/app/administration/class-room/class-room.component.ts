import { DeleteClassRoomComponent } from './delete-class-room/delete-class-room.component';
import { EtModalAlertComponent } from './../et-modal-alert/et-modal-alert.component';
import { Actor } from './../../models/actor';
import { AddRespoComponent } from './add-respo/add-respo.component';
import { AddClassRoomComponent } from './add-class-room/add-class-room.component';
import { ClassRoom } from './../../models/class-room';
import { MatDialog } from '@angular/material/dialog';
import { RessourceService } from './../services/ressource.service';
import { Field } from './../../models/field';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class-room',
  templateUrl: './class-room.component.html',
  styleUrls: ['./class-room.component.scss']
})
export class ClassRoomComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Liste des classes",
      // link: "/settings"
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  isMapMarket: boolean = false;
  page: number = 0;
  size: number = 20;
  fields: Array<Field> = new Array<Field>();
  error: boolean = false;
  messageError = "";
  search ="";

  constructor(public dialog: MatDialog,
    private ressourceService: RessourceService) { }

  ngOnInit(): void {
    this.fetchField();
  }

  addClassRoom(field: Field){
    const dialogRef = this.dialog.open(AddClassRoomComponent, {
      width: "700px",
      data : {
        field: field
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: ClassRoom) => {
      if (result) {

        const index = this.fields.findIndex((elmt) => {
          return (elmt.id == field.id);
        });
        if (index != -1) {
          // this.fields[index].classRoom;
          if(this.fields[index].classRoom == null) this.fields[index].classRoom = [];
          this.fields[index].classRoom?.push(result);
        }
      }
    });
  }

  editClassRoom(classRoom : ClassRoom, field: Field){
    const dialogRef = this.dialog.open(AddClassRoomComponent, {
      width: "700px",
      data : {
        field: field,
        classRoom: classRoom
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: ClassRoom) => {
      if (result) {

        const index = this.fields.findIndex((elmt) => {
          return (elmt.id == field.id);
        });
        if (index != -1) {
          const indexClass = this.fields[index].classRoom?.findIndex((elmt) => {
            return (elmt.id == classRoom.id);
          });
          if (indexClass != -1) {
            this.fields[index].classRoom![indexClass!] = result;
          }
        }
      }
    });
  }

  deleteClassRoom(classRoom : ClassRoom, field: Field){
    const alertDialogRef = this.dialog.open(EtModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment supprimer cette classe?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(DeleteClassRoomComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ classRoomId: classRoom.id})),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {

            const index = this.fields.findIndex((elmt) => {
              return (elmt.id == field.id);
            });
            if (index != -1) {
              const indexClass = this.fields[index].classRoom?.findIndex((elmt) => {
                return (elmt.id == classRoom.id);
              });
              if (indexClass != -1) {
                this.fields[index].classRoom!.splice(indexClass!, 1);
              }
            }
          }
        });
      }
    });
  }

  addClassRoomResp(classRoom : ClassRoom, field: Field){
    // console.log(classRoom);
    const dialogRef = this.dialog.open(AddRespoComponent, {
      width: "700px",
      data : {
        field: field,
        classRoom: classRoom
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Actor) => {
      if (result) {
        const index = this.fields.findIndex((elmt) => {
          return (elmt.id == field.id);
        });
        if (index != -1) {

          const indexClass = this.fields[index].classRoom?.findIndex((elmt) => {
            return (elmt.id == classRoom.id);
          });
          if (indexClass != -1) {
            if(this.fields[index].classRoom![indexClass!].classRepresentative == null) {
              this.fields[index].classRoom![indexClass!].classRepresentative = [];
            }

            this.fields[index].classRoom![indexClass!].classRepresentative.push(result);
          }
        }
      }
    });
  }


  fetchField(){
    this.ressourceService.getField(this.search).subscribe(
      (response)=>{
        this.fields = response;
        this.fields .forEach(element => {
          this.ressourceService.getClassRoom(element.id).subscribe(
            (response)=>{
              element.classRoom = response;
            }
          );
        });
        this.isLoading = false;
        this.isTimeout = false;
      },
      (error)=>{
        this.error = true;
        this.isLoading = false;
        this.catchError(error);
      }
    );
  }

  loadClassRoom(field: Field){
    this.ressourceService.getClassRoom(field.id).subscribe(
      (response)=>{
        field.classRoom = response;
      }
    );
  }

  relaod() {
    this.isReloading = true;
    this.fetchField();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
