import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassRoomItemComponent } from './class-room-item.component';

describe('ClassRoomItemComponent', () => {
  let component: ClassRoomItemComponent;
  let fixture: ComponentFixture<ClassRoomItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassRoomItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassRoomItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
