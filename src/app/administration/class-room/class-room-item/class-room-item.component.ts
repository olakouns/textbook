import { Years } from './../../../models/years.enum';
import { ClassRoom } from './../../../models/class-room';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-class-room-item',
  templateUrl: './class-room-item.component.html',
  styleUrls: ['./class-room-item.component.scss']
})
export class ClassRoomItemComponent implements OnInit {


  @Input() classRoom: ClassRoom;
  @Output() editClassRoom: EventEmitter<void> = new EventEmitter<void>();
  @Output() addClassRoomResp: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteClassRoom: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }


  formatYear(){
    let value = "";
    switch (this.classRoom.years) {
      case Years.THIRST_YEAR:
        value = "Première";
        break;
      case Years.SECOND_YEAR:
        value = "Deuxième";
        break;
      case Years.THIRD_YEAR:
        value = "Troisième";
        break;

      default:
        break;
    }

    return value;
  }

}
