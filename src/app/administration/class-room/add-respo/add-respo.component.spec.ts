import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRespoComponent } from './add-respo.component';

describe('AddRespoComponent', () => {
  let component: AddRespoComponent;
  let fixture: ComponentFixture<AddRespoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRespoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRespoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
