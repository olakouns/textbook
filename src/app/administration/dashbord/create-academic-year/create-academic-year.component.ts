import { AcademicYear } from './../../../models/academic-year';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-create-academic-year',
  templateUrl: './create-academic-year.component.html',
  styleUrls: ['./create-academic-year.component.scss']
})
export class CreateAcademicYearComponent implements OnInit {
  title: String = "Définir l'année";
  validateText: String = "Ajouter";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode!: String;
  error: boolean = false;
  errorMessage: string = "";
  academicYear: AcademicYear;

  constructor(private formBuilder: FormBuilder,
    private ressourceService : RessourceService,
    public dialogRef: MatDialogRef<CreateAcademicYearComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

      this.academicYear = new AcademicYear();
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      startedDate: ['', Validators.required],
      endedDate: ['', Validators.required]
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    this.ressourceService.createAcademicYear(this.academicYear).subscribe(
      (res)=>{
        this.dialogRef.close(res);
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        this.error = true;
        this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
      }
    );
  }

}
