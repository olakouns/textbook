import { ActorService } from './../services/actor.service';
import { StatByField } from './../../models/stat-by-field';
import { StatFirst } from './../../models/stat-first';
import { AcademicYear } from './../../models/academic-year';
import { RessourceService } from './../services/ressource.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateAcademicYearComponent } from './create-academic-year/create-academic-year.component';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.scss']
})
export class DashbordComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Tableau de bord",
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  page: number = 0;
  size: number = 20;
  // pageMarkets: Page<Market> = new Page<Market>();
  // markets: Array<Market> = new Array<Market>();
  error: boolean = false;
  messageError = "";
  currentAcademicYear: AcademicYear;
  hasCurrentYear = false;
  statFirst: StatFirst = new StatFirst();
  statByFields: Array<StatByField> = new Array<StatByField>();

  constructor(public dialog: MatDialog,
    private ressourceService: RessourceService,
    public actorService: ActorService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer) {
      iconRegistry.addSvgIcon(
        'filière',
        sanitizer.bypassSecurityTrustResourceUrl(
          'assets/images/filière.svg'
        )
      );
  }

  ngOnInit(): void {
    this.fecthCurrentYear();
    this.getStatFirst();
    this.fectStatByField();
  }

  getStatFirst(){
    this.ressourceService.getStatFirst().subscribe(
      (response)=>{
        this.statFirst = response;
      }
    );
  }


  fecthCurrentYear(){
    this.isLoading = true;
    this.ressourceService.getCurrentAcademicYear().subscribe(
      (response)=>{
        this.isLoading = false;
        this.currentAcademicYear = response;
        this.hasCurrentYear = true;
      },
      (error)=>{
        // if (error.status) {
        //   this.currentAcademicYear = null;
        // }
        this.isLoading = false;
        this.error = true;
        this.messageError = "There are many error here";

      }
    );
  }

  fectStatByField(){
    this.ressourceService.getAllStatByField().subscribe(
      (response)=>{
        this.statByFields = response;
        console.log(this.statByFields);
      }
    );
  }



  createAcademicYear(){
    const dialogRef = this.dialog.open(CreateAcademicYearComponent, {
      width: "700px",
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: AcademicYear) => {
      if (result) {
        this.currentAcademicYear = result;
      }
    });
  }

  relaod() {
    this.isReloading = true;
    // this.fetchMarket();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
