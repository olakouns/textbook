
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatBadgeModule} from '@angular/material/badge';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { AdministrationRoutingModule } from './administration-routing.module';
import { EtAdminComponent } from './et-admin/et-admin.component';
import { FieldComponent } from './field/field.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import { SubjectComponent } from './subject/subject.component';
import { EditSubjectComponent } from './subject/edit-subject/edit-subject.component';
import { EtDialogueCroquisComponent } from './et-dialogue-croquis/et-dialogue-croquis.component';
import { DeleteSubjectComponent } from './subject/delete-subject/delete-subject.component';
import { EtModalAlertComponent } from './et-modal-alert/et-modal-alert.component';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { SubjectItemComponent } from './subject/subject-item/subject-item.component';
import { AddFieldComponent } from './field/add-field/add-field.component';
import { DeleteFieldComponent } from './field/delete-field/delete-field.component';
import { FiledItemComponent } from './field/filed-item/filed-item.component';
import { AddOptionComponent } from './field/add-option/add-option.component';
import { DeleteOptionComponent } from './field/delete-option/delete-option.component';
import { OptionItemComponent } from './field/option-item/option-item.component';
import { TeacherComponent } from './teacher/teacher.component';
import { AddTeacherComponent } from './teacher/add-teacher/add-teacher.component';
import { DeleteTeacherComponent } from './teacher/delete-teacher/delete-teacher.component';
import { TeacherItemComponent } from './teacher/teacher-item/teacher-item.component';
import { AssignHeadComponent } from './teacher/assign-head/assign-head.component';
import { RemoveAssigningComponent } from './teacher/remove-assigning/remove-assigning.component';
import { CreateAcademicYearComponent } from './dashbord/create-academic-year/create-academic-year.component';
import { ClassRoomComponent } from './class-room/class-room.component';
import { ClassRoomItemComponent } from './class-room/class-room-item/class-room-item.component';
import { AddClassRoomComponent } from './class-room/add-class-room/add-class-room.component';
import { DeleteClassRoomComponent } from './class-room/delete-class-room/delete-class-room.component';
import { AddRespoComponent } from './class-room/add-respo/add-respo.component';


@NgModule({
  declarations: [
    EtAdminComponent,
    FieldComponent,
    DashbordComponent,
    SubjectComponent,
    EditSubjectComponent,
    EtDialogueCroquisComponent,
    DeleteSubjectComponent,
    EtModalAlertComponent,
    SubjectItemComponent,
    AddFieldComponent,
    DeleteFieldComponent,
    FiledItemComponent,
    AddOptionComponent,
    DeleteOptionComponent,
    OptionItemComponent,
    TeacherComponent,
    AddTeacherComponent,
    DeleteTeacherComponent,
    TeacherItemComponent,
    AssignHeadComponent,
    RemoveAssigningComponent,
    CreateAcademicYearComponent,
    ClassRoomComponent,
    ClassRoomItemComponent,
    AddClassRoomComponent,
    DeleteClassRoomComponent,
    AddRespoComponent,
  ],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    NgxPermissionsModule.forChild(),
    MatCardModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    // TranslateModule,
    // MatPasswordStrengthModule,
    MatSelectModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatBadgeModule,
    MatDialogModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientJsonpModule,
    HttpClientModule,
    MatAutocompleteModule
  ],
  exports: [EtAdminComponent, EtModalAlertComponent, EtDialogueCroquisComponent],

  providers: [
    MatDatepickerModule,
  ],
  entryComponents: [
    EditSubjectComponent,
    DeleteSubjectComponent,
    AddFieldComponent,
    DeleteFieldComponent,
    DeleteOptionComponent,
    AddOptionComponent,
    AddTeacherComponent,
    DeleteTeacherComponent,
    AssignHeadComponent,
    RemoveAssigningComponent,
    CreateAcademicYearComponent,
    AddRespoComponent,
    DeleteClassRoomComponent
  ]
})
export class AdministrationModule { }
