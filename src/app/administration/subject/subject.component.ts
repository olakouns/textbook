import { DeleteSubjectComponent } from './delete-subject/delete-subject.component';
import { EtModalAlertComponent } from './../et-modal-alert/et-modal-alert.component';
import { FormBuilder, Validators } from '@angular/forms';
import { RessourceService } from './../services/ressource.service';
import { Page } from './../../payload/page';
import { Subject } from './../../models/subject';
import { EditSubjectComponent } from './edit-subject/edit-subject.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Liste des matières",
      // link: "/settings"
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  page: number = 0;
  size: number = 20;
  pageSubject: Page<Subject> = new Page<Subject>();
  subjects: Array<Subject> = new Array<Subject>();
  error: boolean = false;
  messageError = "";
  subjectSubscription!: Subscription;
  search = "";
  form = this.formBuilder.group({
    search: ["", [Validators.required]],
  });




  constructor(private formBuilder: FormBuilder,
    public dialog: MatDialog, private ressourceService: RessourceService) { }

  ngOnInit(): void {
    this.fetchSubjet();
    // this.subjects = this.staticVale;

  }

  filterSubects(event: any) {


  }

  createSubject(){
    const dialogRef = this.dialog.open(EditSubjectComponent, {
      width: "700px",
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Subject) => {
      if (result) {
        this.subjects.push(result);
      }
    });
  }

  editSubject(subject: Subject){
    const dialogRef = this.dialog.open(EditSubjectComponent, {
      width: "700px",
      data: subject,
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Subject) => {
      if (result) {
        const index = this.subjects.findIndex((elmt) => {
          return (elmt.id == subject.id);
        });
        if (index != -1) {
          this.subjects[index] = result;
        }
      }
    });
  }

  deleteSubject(subject: Subject){
    const alertDialogRef = this.dialog.open(EtModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment supprimer cette matiere?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(DeleteSubjectComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ subjectId: subject.id})),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const index = this.subjects.findIndex((elmt) => {
              return (elmt.id == subject.id);
            });
            this.subjects.splice(index!, 1);
          }
        });
      }
    });
  }

  setPage(event : any) {
    if (event.pageSize != this.size) {
      this.size = event.pageSize;
      this.page = 0;
    } else {
      this.page = event.pageIndex;
    }
    this.fetchSubjet();
  }

  fetchSubjet(){
    this.isLoading = true;
    this.ressourceService.getSubjecs(this.search, this.page, this.size).subscribe(
      (response)=>{
        this.pageSubject = response;
        this.subjects = response.content;
        this.isLoading = false;
        this.isTimeout = false;
      },
      (error)=>{
        this.error = true;
        this.isLoading = false;
        this.catchError(error);
      }
    );
  }

  relaod() {
    this.isReloading = true;
    // this.fetchMarket();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
