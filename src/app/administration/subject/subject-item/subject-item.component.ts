import { Subject } from './../../../models/subject';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-subject-item',
  templateUrl: './subject-item.component.html',
  styleUrls: ['./subject-item.component.scss']
})
export class SubjectItemComponent implements OnInit {
  @Input() subject!: Subject;
  @Output() editSubject: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteSubject: EventEmitter<void> = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

}
