import { RessourceService } from './../../services/ressource.service';
import { Subject } from './../../../models/subject';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-subject',
  templateUrl: './edit-subject.component.html',
  styleUrls: ['./edit-subject.component.scss']
})
export class EditSubjectComponent implements OnInit {
  title: String = "Ajouter une matière";
  validateText: String = "Valider";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode!: String;
  error: boolean = false;
  errorMessage: string = "";
  subject!: Subject;

  constructor(private formBuilder: FormBuilder,
    private ressourceService : RessourceService,
    public dialogRef: MatDialogRef<EditSubjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      if (this.data == null) {
        this.mode = "new";
        this.subject = new Subject();
      } else {
        this.mode = "edit";
        this.title  = "Modifier une matière";
        this.subject = Object.assign({}, this.data);
      }
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required]
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    // this.dialogRef.close(this.subject);
    if (this.mode === "new") {
      this.ressourceService.createSubject(this.subject)
        .subscribe(
          (res: Subject) => {
            this.dialogRef.close(res);
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.error = true;
            this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
          });
    } else if (this.mode === "edit") {
      this.ressourceService.updateSubject(this.subject)
        .subscribe(
          (res: Subject) => {
            this.dialogRef.close(res);
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.error = true;
            this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
          });
    }
  }

}
