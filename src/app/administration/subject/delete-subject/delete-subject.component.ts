import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-delete-subject',
  templateUrl: './delete-subject.component.html',
  styleUrls: ['./delete-subject.component.scss']
})
export class DeleteSubjectComponent implements OnInit {

  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<DeleteSubjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.removeSubject();
  }

  removeSubject() {
    this.ressourceService
      .deleteSubject(this.data.subjectId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }
}
