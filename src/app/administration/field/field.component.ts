import { DeleteFieldComponent } from './delete-field/delete-field.component';
import { EtModalAlertComponent } from './../et-modal-alert/et-modal-alert.component';
import { Field } from './../../models/field';
import { RessourceService } from './../services/ressource.service';
import { MatDialog } from '@angular/material/dialog';
import { AddFieldComponent } from './add-field/add-field.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {

  linkTree: Array<any> = [
    {
      name: "Liste des filières",
      // link: "/settings"
    }
  ];
  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  isMapMarket: boolean = false;
  page: number = 0;
  size: number = 20;
  fields: Array<Field> = new Array<Field>();
  error: boolean = false;
  messageError = "";
  search ="";



  constructor(public dialog: MatDialog,
    private ressourceService: RessourceService) { }

  ngOnInit(): void {
    this.fetchField();
    // this.fields = this.staticVale;
  }

  createField(){
    const dialogRef = this.dialog.open(AddFieldComponent, {
      width: "700px",
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Field) => {
      if (result) {
        this.fields.push(result);
      }
    });
  }

  editField(field: Field){
    const dialogRef = this.dialog.open(AddFieldComponent, {
      width: "700px",
      data: field,
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Field) => {
      if (result) {
        const index = this.fields.findIndex((elmt) => {
          return (elmt.id == field.id);
        });
        if (index != -1) {
          this.fields[index] = result;
        }
      }
    });
  }

  deleteField(field: Field){
    const alertDialogRef = this.dialog.open(EtModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment retirer cette filière?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(DeleteFieldComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ fieldId: field.id})),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const index = this.fields.findIndex((elmt) => {
              return (elmt.id == field.id);
            });
            this.fields.splice(index!, 1);
          }
        });
      }
    });
  }

  fetchField(){
    this.isLoading = true;
    this.ressourceService.getField(this.search).subscribe(
      (response)=>{
        this.fields = response;
        this.isLoading = false;
        this.isTimeout = false;
      },
      (error)=>{
        this.error = true;
        this.isLoading = false;
        this.catchError(error);
      }
    );
  }

  relaod() {
    this.isReloading = true;
    // this.fetchMarket();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
