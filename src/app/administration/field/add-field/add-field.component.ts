import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { Field } from './../../../models/field';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-add-field',
  templateUrl: './add-field.component.html',
  styleUrls: ['./add-field.component.scss']
})
export class AddFieldComponent implements OnInit {
  title: String = "Ajouter une filière";
  validateText: String = "Ajouter";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode!: String;
  error: boolean = false;
  errorMessage: string = "";
  field!: Field;

  constructor(private formBuilder: FormBuilder,
    private ressourceService : RessourceService,
    public dialogRef: MatDialogRef<AddFieldComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      if (this.data == null) {
        this.mode = "new";
        this.field = new Field();
      } else {
        this.mode = "edit";
        this.title  = "Modifier une matière";
        this.field = Object.assign({}, this.data);
      }
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      code: ['', Validators.required],
      name: ['', Validators.required]
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    // this.dialogRef.close(this.field);
    if (this.mode === "new") {
      this.ressourceService.createField(this.field)
        .subscribe(
          (res: Field) => {
            this.dialogRef.close(res);
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.error = true;
            this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
          });
    } else if (this.mode === "edit") {
      this.ressourceService.updateField(this.field)
        .subscribe(
          (res: Field) => {
            this.dialogRef.close(res);
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.error = true;
            this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
          });
    }
  }

}
