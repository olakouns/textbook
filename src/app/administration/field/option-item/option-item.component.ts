import { Option } from './../../../models/option';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-option-item',
  templateUrl: './option-item.component.html',
  styleUrls: ['./option-item.component.scss']
})
export class OptionItemComponent implements OnInit {
  @Input() option!: Option;
  @Output() editOption: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteOption: EventEmitter<void> = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

}
