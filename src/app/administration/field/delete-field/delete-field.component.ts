import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-delete-field',
  templateUrl: './delete-field.component.html',
  styleUrls: ['./delete-field.component.scss']
})
export class DeleteFieldComponent implements OnInit {

  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<DeleteFieldComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    // this.dialogRef.close("response");
    this.removeField();
  }

  removeField() {
    this.ressourceService
      .deleteField(this.data.fieldId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }


}
