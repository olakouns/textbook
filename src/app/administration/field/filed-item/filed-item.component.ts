import { DeleteOptionComponent } from './../delete-option/delete-option.component';
import { EtModalAlertComponent } from './../../et-modal-alert/et-modal-alert.component';
import { AddOptionComponent } from './../add-option/add-option.component';
import { RessourceService } from './../../services/ressource.service';
import { MatDialog } from '@angular/material/dialog';
import { Option } from './../../../models/option';
import { Field } from './../../../models/field';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filed-item',
  templateUrl: './filed-item.component.html',
  styleUrls: ['./filed-item.component.scss']
})
export class FiledItemComponent implements OnInit {
  @Input() field!: Field;
  @Output() editField: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteField: EventEmitter<void> = new EventEmitter<void>();

  constructor(public dialog: MatDialog,
    private ressourceService: RessourceService) { }

  ngOnInit(): void {
  }


  addOption(){
    const dialogRef = this.dialog.open(AddOptionComponent, {
      width: "700px",
      data: {
        fieldId: this.field.id
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Field) => {
      if (result) {
        if (this.field.options == null) this.field.options = [];
        this.field?.options!.push(result);
      }
    });
  }

  editOption(option: Option){
    const dialogRef = this.dialog.open(AddOptionComponent, {
      width: "700px",
      data: {
        fieldId: this.field.id,
        option: option
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Option) => {
      if (result) {
        const index = this.field.options!.findIndex((elmt) => {
          return (elmt.id == option.id);
        });
        if (index != -1) {
          this.field.options![index] = result;
        }
      }
    });
  }


  deleteOption(option: Option){
    const alertDialogRef = this.dialog.open(EtModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment retirer cette options de la liste ?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(DeleteOptionComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ fieldId: this.field.id, optionId: option.id })),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const index = this.field.options?.findIndex((elmt) => {
              return (elmt.id == option.id);
            });
            this.field.options?.splice(index!, 1);
          }
        });
      }
    });
  }



}
