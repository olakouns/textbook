import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiledItemComponent } from './filed-item.component';

describe('FiledItemComponent', () => {
  let component: FiledItemComponent;
  let fixture: ComponentFixture<FiledItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiledItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiledItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
