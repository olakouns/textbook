import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-delete-option',
  templateUrl: './delete-option.component.html',
  styleUrls: ['./delete-option.component.scss']
})
export class DeleteOptionComponent implements OnInit {
  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<DeleteOptionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    // this.dialogRef.close("ok");
    this.removeOption();
  }

  removeOption() {
    this.ressourceService
      .deleteOption(this.data.fieldId, this.data.optionId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }

}
