import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from './../../services/ressource.service';
import { Field } from './../../../models/field';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Option } from './../../../models/option';
import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';

@Component({
  selector: 'app-add-option',
  templateUrl: './add-option.component.html',
  styleUrls: ['./add-option.component.scss']
})
export class AddOptionComponent implements OnInit {

  title: String = "Ajouter une option";
  validateText: String = "Ajouter";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode!: String;
  error: boolean = false;
  errorMessage: string = "";
  fieldId!: number;
  option!: Option;

  constructor(private formBuilder: FormBuilder,
    private ressourceService : RessourceService,
    public dialogRef: MatDialogRef<AddOptionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      if (this.data.option == null) {
        this.mode = "new";
        this.fieldId = this.data.fieldId;
        this.option = new Option();
      } else {
        this.mode = "edit";
        this.title  = "Modifier une option";
        this.fieldId = this.data.fieldId;
        this.option = Object.assign({}, this.data.option);
      }
     }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required]
    });
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    // this.dialogRef.close(this.option);

    if (this.mode === "new") {
      this.ressourceService.createOption(this.fieldId, this.option)
        .subscribe(
          (res: Option) => {
            this.dialogRef.close(res);
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.error = true;
            this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
          });
    } else if (this.mode === "edit") {
      this.ressourceService.updateOption(this.fieldId, this.option.id, this.option)
        .subscribe(
          (res: Option) => {
            this.dialogRef.close(res);
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.error = true;
            this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
          });
    }
    // createOption
  }
}
