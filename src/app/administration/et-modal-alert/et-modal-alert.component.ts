import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-et-modal-alert',
  templateUrl: './et-modal-alert.component.html',
  styleUrls: ['./et-modal-alert.component.scss']
})
export class EtModalAlertComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EtModalAlertComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}
