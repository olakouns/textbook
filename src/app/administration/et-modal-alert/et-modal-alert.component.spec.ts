import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtModalAlertComponent } from './et-modal-alert.component';

describe('EtModalAlertComponent', () => {
  let component: EtModalAlertComponent;
  let fixture: ComponentFixture<EtModalAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtModalAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtModalAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
