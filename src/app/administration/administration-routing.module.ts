import { TeacherComponent } from './teacher/teacher.component';
import { SubjectComponent } from './subject/subject.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import { FieldComponent } from './field/field.component';
import { ClassRoomComponent } from './class-room/class-room.component';
import { EtAdminComponent } from './et-admin/et-admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: "", redirectTo: 'dashbord' },
  { path: "dashbord", component: DashbordComponent },
  { path: "class-room", component: ClassRoomComponent },
  { path: "field", component: FieldComponent },
  { path: "subject", component: SubjectComponent },
  { path: "actors", component: TeacherComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
