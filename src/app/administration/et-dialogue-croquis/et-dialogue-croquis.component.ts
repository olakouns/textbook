import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-et-dialogue-croquis',
  templateUrl: './et-dialogue-croquis.component.html',
  styleUrls: ['./et-dialogue-croquis.component.scss']
})
export class EtDialogueCroquisComponent implements OnInit {

  @Input() title!: String;
  @Input() validateText!: String;
  @Input() data: any;
  @Input() form!: FormGroup;
  @Input() isLoading!: boolean;
  @Input() isTimeout: boolean = false;
  @Input() isReloading: boolean = false;
  @Output() onReload: EventEmitter<void> = new EventEmitter<void>();
  @Output() onFormSubmited: EventEmitter<any> = new EventEmitter<any>();

  constructor(public dialogRef: MatDialogRef<EtDialogueCroquisComponent>) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.onFormSubmited.emit(this.data);
  }

}
