import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtDialogueCroquisComponent } from './et-dialogue-croquis.component';

describe('EtDialogueCroquisComponent', () => {
  let component: EtDialogueCroquisComponent;
  let fixture: ComponentFixture<EtDialogueCroquisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtDialogueCroquisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtDialogueCroquisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
