import { Media } from './../models/media';
export class AddActor {
  firstName: string;
  lastName: string;
  phone: string;
  title: string;
  address: string;
  email: string;
  password: string;
  media: Media;
}
