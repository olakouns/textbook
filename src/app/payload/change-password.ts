export class ChangePassword {
  email: string;
  token: string;
  password: string;
  passwordNew: string;
}
