import { AcademicYear } from './academic-year';
import { ClassRoom } from './class-room';

export class CText {
  id: string;
  classRoom: ClassRoom;
  academicYear: AcademicYear;
}
