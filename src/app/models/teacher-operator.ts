import { SubjectField } from './subject-field';
import { Field } from './field';
import { ClassRoom } from './class-room';

export class TeacherOperator {
  programmeCourseId: string;
  subjectField: SubjectField;
  startedDate: Date;
  endedDate: Date;
  field: Field;
  classRoom: ClassRoom;
}
