import { Option } from './option';
import { ClassRoom } from './class-room';
export class Field {
  id: number;
  name: string;
  code: string;
  options?: Array<Option>;
  classRoom?: Array<ClassRoom>;
}
