import { AcademicYear } from './academic-year';
import { SubjectField } from './subject-field';
import { Actor } from './actor';

export class ProgrammeCourse {
  id: string;
  professor: Actor;
  subjectField: SubjectField;
  programmeDuration: number;
  startedDate: Date;
  endedDate: Date;
  status: boolean;
  academicYear: AcademicYear;
  started: boolean;
}
