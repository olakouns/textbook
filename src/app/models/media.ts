import { User } from './user';

export class Media {
  id: number;
  url: string;
  thumbnailUrl: string;
  originalName: string;
  type: string;
  createdAt: string;
  postedBy: User;
  description: string;
}
