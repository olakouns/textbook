
export class AcademicYear {
  id: string;
  name: string;
  startedDate: Date;
  endedDate: Date;
  current: boolean;
}
