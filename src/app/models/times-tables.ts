import { ClassRoom } from './class-room';
import { ProCalendar } from './pro-calendar';
export class TimesTables {
  classRoom: ClassRoom;
  proCalendars: Array<ProCalendar>;
}
