import { Field } from './field';
export class Option {
  id: number;
  code: string;
  name: string;
  field?: Field;
}
