import { AcademicYear } from './academic-year';
import { SubjectField } from './subject-field';
import { TypeDay } from './type-day.enum';

export class ProCalendar {
  id: string;
  typeDay: TypeDay;
  startedHours: Date;
  endedHours: Date;
  duration: number;
  subjectField: SubjectField;
  academicYear: AcademicYear;
}
