import { Semester } from './semester.enum';
import { Field } from './field';
import { AcademicYear } from './academic-year';
import { Subject } from './subject';

export class SubjectField {
  id: string;
  subject: Subject;
  academicYear: AcademicYear;
  field : Field;
  semester: Semester;
}
