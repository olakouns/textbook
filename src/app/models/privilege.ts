import { TypePrivilege } from './type-privilege.enum';

export class Privilege {
  id: number;
  name: TypePrivilege;
  description: String ;
}
