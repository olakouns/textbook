import { Actor } from './actor';
import { SubjectField } from './subject-field';

export class Seance {
  id: string;
  subjectField: SubjectField;
  workDone: string;
  startedDate: Date;
  endedDate: Date;
  supervisor: Actor;
  validate: boolean;
  duration: number;
  day: Date;
}
