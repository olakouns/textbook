import { Privilege } from './privilege';

export class Role {
  id: number;
  name: String;
  description: String;
  privileges: Array<Privilege>;
}
