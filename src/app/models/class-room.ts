import { Years } from './years.enum';
import { Option } from './option';
import { AcademicYear } from './academic-year';
import { Field } from './field';
import { Actor } from "./actor";

export class ClassRoom {
  id: number;
  name: string;
  field: Field;
  classRepresentative: Array<Actor>;
  academicYear: AcademicYear;
  option: Option;
  years: Years;
}
