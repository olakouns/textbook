import { StatusUser } from './status-user.enum';
import { Role } from './role';
export class User {
  id: string;
  name: string;
  username: string;
  email: string;
  password: string;
  roles: Array<Role>
  status: StatusUser;
}
