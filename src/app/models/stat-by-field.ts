import { Field } from './field';
import { Actor } from './actor';

export class StatByField {
  head: Actor;
  field: Field;
  nbrOption: number;
  nbrClass: number;
  nbrClassRoomFirst: number;
  nbrClassRoomSecond: number;
  nbrClassRooThird: number;
}
