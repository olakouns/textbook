import { User } from './user';
import { Media } from './media';
import { TypeRole } from './type-role';

export class Actor {
  id: string;
  user: User;
  firstName: string;
  lastName: string;
  phone: string;
  title: string;
  address: string;
  mediaFile: Media;
  email: string;
  type: TypeRole;
}
