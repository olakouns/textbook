import { Constants } from './constants';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // let header :  HttpHeaders = new HttpHeaders();

    if(this.authService.isAuth()){
      // header.append('Authorization','Bearer ' + this.authService.getAccessToken());
      req = req.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + this.authService.getAccessToken(),
          'Access-Control-Allow-Origin': '' + Constants.UI_URL,
        },
      });
    } else {
      req = req.clone({
        setHeaders: {
          'Access-Control-Allow-Origin': '' + Constants.UI_URL,
        },
      });

    }

    return next.handle(req);
  }
}
