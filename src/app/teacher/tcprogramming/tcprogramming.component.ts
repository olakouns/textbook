import { TeacherService } from './../services/teacher.service';
import { TeacherOperator } from './../../models/teacher-operator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tcprogramming',
  templateUrl: './tcprogramming.component.html',
  styleUrls: ['./tcprogramming.component.scss']
})
export class TCProgrammingComponent implements OnInit {

  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  teacherOperators :  Array<TeacherOperator> = new Array<TeacherOperator>();

  constructor(private teacherService : TeacherService) { }

  ngOnInit(): void {
    this.fecthTeacherOperator();
  }

  fecthTeacherOperator(){
    this.teacherService.getTeacherOperator().subscribe(
      (res)=>{
        this.teacherOperators = res;
      },
      (error)=>{
        console.log(error);
      }
    );
  }

  relaod() {
    this.isReloading = true;
    this.fecthTeacherOperator();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
