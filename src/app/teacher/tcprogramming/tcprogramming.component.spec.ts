import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TCProgrammingComponent } from './tcprogramming.component';

describe('TCProgrammingComponent', () => {
  let component: TCProgrammingComponent;
  let fixture: ComponentFixture<TCProgrammingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TCProgrammingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TCProgrammingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
