import { Seance } from './../../../../models/seance';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HeadClassService } from './../../../../head-class-room/services/head-class.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-validate-tcseance',
  templateUrl: './validate-tcseance.component.html',
  styleUrls: ['./validate-tcseance.component.scss']
})
export class ValidateTCSeanceComponent implements OnInit {

  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private headClassService: HeadClassService,
    public dialogRef: MatDialogRef<ValidateTCSeanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Seance) { }

  ngOnInit(): void {
    this.validateSeance();
  }

  validateSeance(){
    this.headClassService.validateSeance(this.data.id).subscribe(
      (response)=>{
        this.data.validate = true;
        this.dialogRef.close(this.data);
      }
    );
  }

}
