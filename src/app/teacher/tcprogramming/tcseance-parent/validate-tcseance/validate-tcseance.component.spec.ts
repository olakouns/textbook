import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateTCSeanceComponent } from './validate-tcseance.component';

describe('ValidateTCSeanceComponent', () => {
  let component: ValidateTCSeanceComponent;
  let fixture: ComponentFixture<ValidateTCSeanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidateTCSeanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateTCSeanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
