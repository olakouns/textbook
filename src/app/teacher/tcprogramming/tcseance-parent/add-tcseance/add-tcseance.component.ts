import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RessourceService } from '../../../../administration/services/ressource.service';
import { DepartmentService } from '../../../../head-of-departement/services/department.service';
import { HeadClassService } from '../../../../head-class-room/services/head-class.service';
import { Seance } from '../../../../models/seance';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-add-tcseance',
  templateUrl: './add-tcseance.component.html',
  styleUrls: ['./add-tcseance.component.scss']
})
export class AddTCSeanceComponent implements OnInit {

  title: String = "Faire une programmation";
  validateText: String = "Valider";
  form!: FormGroup;
  isLoading: boolean = false;
  isLoadingData: boolean = false;
  isTimeout: boolean = false;
  mode: String;
  error: boolean = false;
  errorMessage: string = "";
  isReloading: boolean = false;
  seance: Seance;
  subjectFieldId: string;

  startedHoursValue= "08:00";
  endedHoursValue= "08:00";

  constructor(private formBuilder: FormBuilder,
    private headClassService : HeadClassService,
    private departmentService: DepartmentService,
    private ressourceService: RessourceService,
    public dialogRef: MatDialogRef<AddTCSeanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.subjectFieldId = this.data.subjectFieldId;
      if (this.data.seance == null) {
        this.mode = "new";
        this.seance = new Seance();
      } else {
        this.mode = "edit";
        this.title  = "Modifier une matière";
        this.seance = Object.assign({}, this.data.seance);
        this.changeHours();
      }
     }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      description: ['', Validators.required],
      startedDate: ['', Validators.required],
      endedDate: ['', Validators.required],
      duration: ['', Validators.required],
    });
  }

  changeHours(){
    this.startedHoursValue = new Date (this.seance.startedDate).getHours() + ":" + new Date (this.seance.startedDate).getMinutes();
    this.endedHoursValue = new Date (this.seance.endedDate).getHours() + ":" + new Date (this.seance.endedDate).getMinutes();
  }

  onSubmit(){
    this.error = false;
    this.isLoading = true;
    this.seance.startedDate = new Date("Tue Jun 15 2021 "+this.startedHoursValue+":00 GMT+0100");
    this.seance.endedDate = new Date("Tue Jun 15 2021 "+this.endedHoursValue+":00 GMT+0100");
    if (this.mode === "new") {
      this.headClassService.addSeance(this.subjectFieldId, this.seance).subscribe(
        (response)=>{
          // console.log(response);
          this.dialogRef.close(response);
          this.isLoading = false;
        },
        (error)=>{
          this.isLoading = false;
          this.error = true;
          this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
        }
      );

    }else if (this.mode === "edit") {
      this.headClassService.updateSeance(this.seance.id, this.seance).subscribe(
        (response)=>{
          console.log(response);
          this.dialogRef.close(response);
          this.isLoading = false;
        },
        (error)=>{
          this.isLoading = false;
          this.error = true;
          this.errorMessage = "Une erreur s'est produite. Veuillez réessayer plus tard";
        }
      );
    }
  }


}
