import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTCSeanceComponent } from './add-tcseance.component';

describe('AddTCSeanceComponent', () => {
  let component: AddTCSeanceComponent;
  let fixture: ComponentFixture<AddTCSeanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTCSeanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTCSeanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
