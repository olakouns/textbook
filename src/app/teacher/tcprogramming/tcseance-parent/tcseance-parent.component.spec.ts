import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TCSeanceParentComponent } from './tcseance-parent.component';

describe('TCSeanceParentComponent', () => {
  let component: TCSeanceParentComponent;
  let fixture: ComponentFixture<TCSeanceParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TCSeanceParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TCSeanceParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
