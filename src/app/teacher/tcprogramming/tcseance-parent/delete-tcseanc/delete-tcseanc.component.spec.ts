import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTCseancComponent } from './delete-tcseanc.component';

describe('DeleteTCseancComponent', () => {
  let component: DeleteTCseancComponent;
  let fixture: ComponentFixture<DeleteTCseancComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteTCseancComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTCseancComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
