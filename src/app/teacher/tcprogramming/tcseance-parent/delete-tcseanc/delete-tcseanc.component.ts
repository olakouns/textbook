import { HeadClassService } from './../../../../head-class-room/services/head-class.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-delete-tcseanc',
  templateUrl: './delete-tcseanc.component.html',
  styleUrls: ['./delete-tcseanc.component.scss']
})
export class DeleteTCseancComponent implements OnInit {

  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private headClassService: HeadClassService,
    public dialogRef: MatDialogRef<DeleteTCseancComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.removeSeance();
  }

  removeSeance() {
    this.headClassService
      .deleteSeance(this.data.seanceId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }

}
