import { TeacherOperator } from './../../../models/teacher-operator';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-programming-item',
  templateUrl: './programming-item.component.html',
  styleUrls: ['./programming-item.component.scss']
})
export class ProgrammingItemComponent implements OnInit {

  @Input() teacherOperator: TeacherOperator;
  constructor(private router: Router) { }

  ngOnInit(): void {
    console.log(this.teacherOperator);
  }

  openDetails(){
    this.router.navigateByUrl(`teacher/seances/details/${this.teacherOperator.subjectField.id}/${this.teacherOperator.programmeCourseId}`);
  }

}
