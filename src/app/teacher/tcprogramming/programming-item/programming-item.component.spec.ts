import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammingItemComponent } from './programming-item.component';

describe('ProgrammingItemComponent', () => {
  let component: ProgrammingItemComponent;
  let fixture: ComponentFixture<ProgrammingItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgrammingItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
