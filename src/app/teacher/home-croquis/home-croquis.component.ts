import { AuthService } from './../../auth.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-croquis',
  templateUrl: './home-croquis.component.html',
  styleUrls: ['./home-croquis.component.scss']
})
export class HomeCroquisComponent implements OnInit {

  @Input() title!: string;
  @Input() linkTree: Array<any> = [];
  @Input() isTimeout = false;
  @Input() isReloading = false;

  links = [
    {
      name: 'Les programmations',
      link: '/teacher/programming',
      icon: 'dashboard',
      permissions: [],
    }
  ];

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  deconnexion(): any {
    this.authService.logout();
  }

}
