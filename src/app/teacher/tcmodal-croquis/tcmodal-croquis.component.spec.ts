import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TCModalCroquisComponent } from './tcmodal-croquis.component';

describe('TCModalCroquisComponent', () => {
  let component: TCModalCroquisComponent;
  let fixture: ComponentFixture<TCModalCroquisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TCModalCroquisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TCModalCroquisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
