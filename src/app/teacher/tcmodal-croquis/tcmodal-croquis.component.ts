import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tcmodal-croquis',
  templateUrl: './tcmodal-croquis.component.html',
  styleUrls: ['./tcmodal-croquis.component.scss']
})
export class TCModalCroquisComponent implements OnInit {

  @Input() title!: String;
  @Input() otherCondition: boolean;
  @Input() validateText!: String;
  @Input() data: any;
  @Input() form!: FormGroup;
  @Input() isLoading!: boolean;
  @Input() isTimeout: boolean = false;
  @Input() isReloading: boolean = false;
  @Output() onReload: EventEmitter<void> = new EventEmitter<void>();
  @Output() onFormSubmited: EventEmitter<any> = new EventEmitter<any>();

  constructor(public dialogRef: MatDialogRef<TCModalCroquisComponent>) { }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.onFormSubmited.emit(this.data);
  }


}
