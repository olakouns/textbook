// import { DeleteTCseancComponent } from './tcprogramming/delete-tcseanc/delete-tcseanc.component';
import { NgxTrumbowygModule } from 'ngx-trumbowyg';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatBadgeModule } from '@angular/material/badge';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeacherRoutingModule } from './teacher-routing.module';
import { HomeCroquisComponent } from './home-croquis/home-croquis.component';
import { TCModalAlertComponent } from './tcmodal-alert/tcmodal-alert.component';
import { TCModalCroquisComponent } from './tcmodal-croquis/tcmodal-croquis.component';
import { TCProgrammingComponent } from './tcprogramming/tcprogramming.component';
import { TCSeanceParentComponent } from './tcprogramming/tcseance-parent/tcseance-parent.component';
import { AddTCSeanceComponent } from './tcprogramming/tcseance-parent/add-tcseance/add-tcseance.component';
import { DeleteTCseancComponent } from './tcprogramming/tcseance-parent/delete-tcseanc/delete-tcseanc.component';
import { ValidateTCSeanceComponent } from './tcprogramming/tcseance-parent/validate-tcseance/validate-tcseance.component';
import { ProgrammingItemComponent } from './tcprogramming/programming-item/programming-item.component';


@NgModule({
  declarations: [
    HomeCroquisComponent,
    TCModalAlertComponent,
    TCModalCroquisComponent,
    TCProgrammingComponent,
    TCSeanceParentComponent,
    AddTCSeanceComponent,
    DeleteTCseancComponent,
    ValidateTCSeanceComponent,
    ProgrammingItemComponent
  ],
  imports: [
    CommonModule,
    TeacherRoutingModule,
    NgxPermissionsModule.forChild(),
    MatCardModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatBadgeModule,
    MatDialogModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientJsonpModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    FormsModule,
    NgxMaterialTimepickerModule,
    FlexLayoutModule,
    NgxTrumbowygModule.withConfig({
      lang: 'fr',
      svgPath: '/assets/ui/icons.svg',
      removeformatPasted: true,
      autogrow: true,
      btns: [
        ['viewHTML'],
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['insertImage', 'upload'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
      ]
    })
  ],
  exports: [TCModalAlertComponent, TCModalCroquisComponent],
  providers: [
    MatDatepickerModule,
    NgxMaterialTimepickerModule
  ],
  entryComponents: [
    AddTCSeanceComponent,
    DeleteTCseancComponent,
    ValidateTCSeanceComponent
  ]

})
export class TeacherModule { }
