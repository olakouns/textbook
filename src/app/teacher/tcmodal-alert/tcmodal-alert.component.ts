import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-tcmodal-alert',
  templateUrl: './tcmodal-alert.component.html',
  styleUrls: ['./tcmodal-alert.component.scss']
})
export class TCModalAlertComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TCModalAlertComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

}
