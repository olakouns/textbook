import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TCModalAlertComponent } from './tcmodal-alert.component';

describe('TCModalAlertComponent', () => {
  let component: TCModalAlertComponent;
  let fixture: ComponentFixture<TCModalAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TCModalAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TCModalAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
