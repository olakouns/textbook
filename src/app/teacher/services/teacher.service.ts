import { Constants } from './../../constants';
import { Observable } from 'rxjs';
import { TeacherOperator } from './../../models/teacher-operator';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  constructor(private httpClient: HttpClient) { }

  getTeacherOperator(): Observable<Array<TeacherOperator>>{
    return this.httpClient.get<Array<TeacherOperator>>(Constants.URL + `head-class/teacher-operators`);

  }
}
