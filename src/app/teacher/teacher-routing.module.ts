import { TCSeanceParentComponent } from './tcprogramming/tcseance-parent/tcseance-parent.component';
import { TCProgrammingComponent } from './tcprogramming/tcprogramming.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "", redirectTo: 'programming' },
  { path: 'programming', component: TCProgrammingComponent },
  { path: 'seances/details/:id/:idProgramm', component: TCSeanceParentComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule { }
