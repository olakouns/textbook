import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-hcmodal-alert',
  templateUrl: './hcmodal-alert.component.html',
  styleUrls: ['./hcmodal-alert.component.scss']
})
export class HCModalAlertComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<HCModalAlertComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }


}
