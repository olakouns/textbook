import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HCModalAlertComponent } from './hcmodal-alert.component';

describe('HCModalAlertComponent', () => {
  let component: HCModalAlertComponent;
  let fixture: ComponentFixture<HCModalAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HCModalAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HCModalAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
