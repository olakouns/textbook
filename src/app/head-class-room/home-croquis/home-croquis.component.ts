import { AuthService } from './../../auth.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-croquis',
  templateUrl: './home-croquis.component.html',
  styleUrls: ['./home-croquis.component.scss']
})
export class HomeCroquisComponent implements OnInit {

  @Input() title!: string;
  @Input() linkTree: Array<any> = [];
  @Input() isTimeout = false;
  @Input() isReloading = false;

  links = [
    {
      name: 'Les programmations',
      link: '/head-of-class/programming',
      icon: 'dashboard',
      permissions: [],
    },
    {
      name: 'Emploi du temps',
      link: '/head-of-class/my-times-table',
      icon: 'myClass',
      permissions: [],
    },
  ];
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  deconnexion(): any {
    this.authService.logout();
  }

}
