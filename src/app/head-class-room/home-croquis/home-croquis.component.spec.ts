import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCroquisComponent } from './home-croquis.component';

describe('HomeCroquisComponent', () => {
  let component: HomeCroquisComponent;
  let fixture: ComponentFixture<HomeCroquisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeCroquisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCroquisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
