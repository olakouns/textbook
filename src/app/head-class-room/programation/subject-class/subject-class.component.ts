import { Router } from '@angular/router';
import { SubjectField } from './../../../models/subject-field';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'src/app/models/subject';

@Component({
  selector: 'app-subject-class',
  templateUrl: './subject-class.component.html',
  styleUrls: ['./subject-class.component.scss']
})
export class SubjectClassComponent implements OnInit {

  @Input() subjectField: SubjectField;
  @Input() programmeCourseId: string;
  @Output() detailsSubjectField: EventEmitter<void> = new EventEmitter<void>();
  subject: Subject;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.subject = this.subjectField.subject;
  }

  openDetails(){
    this.router.navigateByUrl(`head-of-class/seances/details/${this.subjectField.id}/${this.programmeCourseId}`);
  }

}
