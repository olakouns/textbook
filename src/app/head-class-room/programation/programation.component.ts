import { ProgrammeCourse } from './../../models/programme-course';
import { SubjectField } from './../../models/subject-field';
import { HeadClassService } from './../services/head-class.service';
import { Component, OnInit } from '@angular/core';
import { SubjectComponent } from 'src/app/administration/subject/subject.component';

@Component({
  selector: 'app-programation',
  templateUrl: './programation.component.html',
  styleUrls: ['./programation.component.scss']
})
export class ProgramationComponent implements OnInit {

  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  subjectFields :  Array<SubjectField> = new Array<SubjectField>();
  programmeCourses :  Array<ProgrammeCourse> = new Array<ProgrammeCourse>();
  constructor(public headClassService: HeadClassService) { }

  ngOnInit(): void {
    this.fetchSubjectField();
  }

  detailsSubjectField(subjectField : SubjectField){

  }

  fetchSubjectField(){
    this.isLoading = true;
    this.headClassService.getSubjectFieldProgram().subscribe(
      (response)=>{
        this.programmeCourses = response;
        // console.log(this.subjectFields);
        this.isLoading = false;
      },
      (error)=>{

      }
    );
  }

  relaod() {
    this.isReloading = true;
    this.fetchSubjectField();
  }

  catchError(error: any) {
    if (error.status === 0) {
      this.isTimeout = true;
      this.isReloading = false;
    } else {
      this.isTimeout = false;
    }
  }

}
