import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HeadClassService } from 'src/app/head-class-room/services/head-class.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-delete-seance',
  templateUrl: './delete-seance.component.html',
  styleUrls: ['./delete-seance.component.scss']
})
export class DeleteSeanceComponent implements OnInit {

  title: String = "Suppression en cours";
  validateText: String = "OK";
  isLoading: boolean = true;

  constructor(private headClassService: HeadClassService,
    public dialogRef: MatDialogRef<DeleteSeanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.removeSeance();
  }

  removeSeance() {
    this.headClassService
      .deleteSeance(this.data.seanceId)
      .subscribe(
        (response: any) => {
          this.dialogRef.close(response);
        },
        (error) => {
          this.dialogRef.close();
        }
      );
  }

}
