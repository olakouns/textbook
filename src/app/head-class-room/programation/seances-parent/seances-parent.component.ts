import { DeleteSeanceComponent } from './delete-seance/delete-seance.component';
import { HCModalAlertComponent } from './../../hcmodal-alert/hcmodal-alert.component';
import { MatDialog } from '@angular/material/dialog';
import { AddSeanceComponent } from './add-seance/add-seance.component';
import { Seance } from './../../../models/seance';
import { Page } from './../../../payload/page';
import { SubjectField } from './../../../models/subject-field';
import { HeadClassService } from './../../services/head-class.service';
import { ActivatedRoute } from '@angular/router';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-seances-parent',
  templateUrl: './seances-parent.component.html',
  styleUrls: ['./seances-parent.component.scss']
})
export class SeancesParentComponent implements OnInit, AfterViewInit {

  isTimeout: boolean = false;
  isReloading: boolean = false;
  isLoading: boolean = false;
  subjectFieldId : string;
  search: string = "";
  page: number = 0;
  size: number = 10;
  startedDate: number;
  endedDate: number;
  seancesPage: Page<Seance> = new Page<Seance>();
  seances: Array<Seance> = new Array<Seance>();
  displayedColumns: string[] = ['day', 'hours',  'workDone',  'action' , 'validate'];
  dataSource : any;
  //  = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // idProgramm
  programmeCourseId : string;

  constructor(public dialog: MatDialog,
    private route: ActivatedRoute, private headClassService: HeadClassService) { }

  ngOnInit(): void {
    this.subjectFieldId = this.route.snapshot.paramMap.get('id')!;
    this.programmeCourseId = this.route.snapshot.paramMap.get('idProgramm')!;
    this.fetchSeance();
  }

  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
  }

  addSeance(){
    const dialogRef = this.dialog.open(AddSeanceComponent, {
      width: "700px",
      data: {
        subjectFieldId: this.subjectFieldId
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Seance) => {
      if (result) {
       this.seances.push(result);
       this.dataSource.data = this.seances;
      }
    });
  }

  editSeance(element : Seance){
    const dialogRef = this.dialog.open(AddSeanceComponent, {
      width: "700px",
      data: {
        subjectFieldId: this.subjectFieldId,
        seance: element
      },
      autoFocus: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result: Seance) => {
      if (result) {
        const index = this.seances.findIndex((elmt) => {
          return (elmt.id == element.id);
        });
        if (index != -1) {
          this.seances[index] = result;
        }
      //  this.seances.push(result);
       this.dataSource.data = this.seances;
      }
    });
  }

  deleteSeance(element : Seance){
    const alertDialogRef = this.dialog.open(HCModalAlertComponent, {
      width: "500px",
      data: JSON.parse(JSON.stringify({ title: 'Suppression', message: 'Voulez-vous vraiment supprimer cette seance?' })),
      autoFocus: false,
      disableClose: true,
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const dialogRef = this.dialog.open(DeleteSeanceComponent, {
          width: "300px",
          data: JSON.parse(JSON.stringify({ seanceId: element.id})),
          autoFocus: false,
          disableClose: true,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const index = this.seances.findIndex((elmt) => {
              return (elmt.id == element.id);
            });
            this.seances.splice(index!, 1);
            this.dataSource.data = this.seances;
          }
        });
      }
    });
  }

  fetchSeance(){
    this.isLoading = true;
    this.headClassService.getSeance(this.programmeCourseId, this.search, this.page, this.size, this.startedDate, this.endedDate).subscribe(
      (response)=>{
        this.seancesPage = response;
        this.seances = this.seancesPage.content;
        this.dataSource = new MatTableDataSource<Seance>(this.seances);
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
        // console.log(this.seances);
      },
      (error)=>{

      }
    );
  }

}
