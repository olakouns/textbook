import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeancesParentComponent } from './seances-parent.component';

describe('SeancesParentComponent', () => {
  let component: SeancesParentComponent;
  let fixture: ComponentFixture<SeancesParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeancesParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeancesParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
