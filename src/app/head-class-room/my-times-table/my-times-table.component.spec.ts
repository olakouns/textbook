import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTimesTableComponent } from './my-times-table.component';

describe('MyTimesTableComponent', () => {
  let component: MyTimesTableComponent;
  let fixture: ComponentFixture<MyTimesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyTimesTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTimesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
