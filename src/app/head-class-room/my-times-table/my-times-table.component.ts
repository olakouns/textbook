import { ProCalendar } from './../../models/pro-calendar';
import { TypeDay } from './../../models/type-day.enum';
import { HeadClassService } from 'src/app/head-class-room/services/head-class.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-times-table',
  templateUrl: './my-times-table.component.html',
  styleUrls: ['./my-times-table.component.scss']
})
export class MyTimesTableComponent implements OnInit {

  proCalendars: Array<ProCalendar> = new Array<ProCalendar> ();
  dayValue = [
    {
      key :  TypeDay.MONDAY,
      value: "Lundi"
    },
    {
      key :  TypeDay.TUESDAY,
      value: "Mardi"
    },
    {
      key :  TypeDay.WEDNESDAY,
      value: "Mercredi"
    },
    {
      key :  TypeDay.THURSDAY,
      value: "Jeudi"
    },
    {
      key :  TypeDay.FRIDAY,
      value: "Vendredi"
    },
    {
      key :  TypeDay.SATURDAY,
      value: "Samedi"
    }
  ];
  showProCalendars: ProCalendar;
  constructor(private headClassService : HeadClassService) { }

  ngOnInit(): void {
    this.fetchMycalendar();
  }

  fetchMycalendar(){
    this.headClassService.getMyCalendar().subscribe(
      (response)=>{
        this.proCalendars = response;
      }
    );
  }

  refactoring(proCalendar: Array<ProCalendar>, day: TypeDay){
    let value = new Array<ProCalendar>();
    proCalendar.forEach(element => {
      if (element.typeDay == day ) {
        value.push(element);
      }
    });

    return value;

  }

  formatValue(value: any){
    let out = "";
    switch (value) {
      case TypeDay.MONDAY:
        out =  "Lundi";
        break;
      case TypeDay.TUESDAY:
        out =  "Mardi";
        break;
      case TypeDay.WEDNESDAY:
        out =  "Mercredi";
        break;
      case TypeDay.THURSDAY:
        out =  "Jeudi";
        break;
      case TypeDay.FRIDAY:
        out =  "Vendredi";
        break;
      case TypeDay.SATURDAY:
        out =  "Samedi";
        break;

      default:
        break;
    }
    return out;
  }

}
