import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatBadgeModule } from '@angular/material/badge';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeadClassRoomRoutingModule } from './head-class-room-routing.module';
import { HCModalAlertComponent } from './hcmodal-alert/hcmodal-alert.component';
import { HCModalCroquisComponent } from './hcmodal-croquis/hcmodal-croquis.component';
import { HomeCroquisComponent } from './home-croquis/home-croquis.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProgramationComponent } from './programation/programation.component';
import { MyTimesTableComponent } from './my-times-table/my-times-table.component';
import { SubjectClassComponent } from './programation/subject-class/subject-class.component';
import { SeancesParentComponent } from './programation/seances-parent/seances-parent.component';
import { AddSeanceComponent } from './programation/seances-parent/add-seance/add-seance.component';
import { SeanceItemComponent } from './programation/seances-parent/seance-item/seance-item.component';
import { NgxTrumbowygModule } from 'ngx-trumbowyg';
import { DeleteSeanceComponent } from './programation/seances-parent/delete-seance/delete-seance.component';


@NgModule({
  declarations: [
    HCModalAlertComponent,
    HCModalCroquisComponent,
    HomeCroquisComponent,
    ProgramationComponent,
    MyTimesTableComponent,
    SubjectClassComponent,
    SeancesParentComponent,
    AddSeanceComponent,
    SeanceItemComponent,
    DeleteSeanceComponent
  ],
  imports: [
    CommonModule,
    HeadClassRoomRoutingModule,
    NgxPermissionsModule.forChild(),
    MatCardModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatBadgeModule,
    MatDialogModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientJsonpModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    FormsModule,
    NgxMaterialTimepickerModule,
    FlexLayoutModule,
    NgxTrumbowygModule.withConfig({
      lang: 'fr',
      svgPath: '/assets/ui/icons.svg',
      removeformatPasted: true,
      autogrow: true,
      btns: [
        ['viewHTML'],
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['insertImage', 'upload'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
      ]
    })
  ],
  exports: [HCModalAlertComponent, HCModalCroquisComponent],
  providers: [
    MatDatepickerModule,
    NgxMaterialTimepickerModule
  ],
  entryComponents: [
    AddSeanceComponent,
    DeleteSeanceComponent
  ]
})
export class HeadClassRoomModule { }
