import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hcmodal-croquis',
  templateUrl: './hcmodal-croquis.component.html',
  styleUrls: ['./hcmodal-croquis.component.scss']
})
export class HCModalCroquisComponent implements OnInit {

  @Input() title!: String;
  @Input() otherCondition: boolean;
  @Input() validateText!: String;
  @Input() data: any;
  @Input() form!: FormGroup;
  @Input() isLoading!: boolean;
  @Input() isTimeout: boolean = false;
  @Input() isReloading: boolean = false;
  @Output() onReload: EventEmitter<void> = new EventEmitter<void>();
  @Output() onFormSubmited: EventEmitter<any> = new EventEmitter<any>();

  constructor(public dialogRef: MatDialogRef<HCModalCroquisComponent>) { }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.onFormSubmited.emit(this.data);
  }


}
