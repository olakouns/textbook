import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HCModalCroquisComponent } from './hcmodal-croquis.component';

describe('HCModalCroquisComponent', () => {
  let component: HCModalCroquisComponent;
  let fixture: ComponentFixture<HCModalCroquisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HCModalCroquisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HCModalCroquisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
