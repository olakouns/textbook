import { ProgrammeCourse } from './../../models/programme-course';
import { ProCalendar } from './../../models/pro-calendar';
import { SubjectField } from './../../models/subject-field';
import { Page } from './../../payload/page';
import { ApiResponse } from './../../payload/api-response';
import { Constants } from './../../constants';
import { Observable } from 'rxjs';
import { Seance } from './../../models/seance';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeadClassService {

  constructor(private httpClient: HttpClient) { }

  addSeance(subjectFieldId : string, seance: Seance): Observable<Seance>{
    let params = new HttpParams()
    .set("subjectFieldId", `${subjectFieldId}`);
    return this.httpClient.post<Seance>(Constants.URL + `head-class/seances`, seance, { params: params });
  }

  updateSeance(seanceId : string, seance: Seance): Observable<Seance>{
    return this.httpClient.put<Seance>(Constants.URL + `head-class/seances/${seanceId}`, seance);
  }

  validateSeance(seanceId : string): Observable<ApiResponse>{
    return this.httpClient.put<ApiResponse>(Constants.URL + `head-class/seances/${seanceId}/validate`, "ok");
  }

  deleteSeance(seanceId : string): Observable<ApiResponse>{
    return this.httpClient.delete<ApiResponse>(Constants.URL + `head-class/seances/${seanceId}`);
  }

  getSeance(subjectFieldId : string, search: string, page: number, size: number, startedDate: number, endedDate: number): Observable<Page<Seance>>{
    let params = new HttpParams()
    .set("subjectFieldId", `${subjectFieldId}`)
    .set("search", `${search}`)
    .set("page", `${page}`)
    .set("size", `${size}`);
    // .set("startedDate", `${startedDate}`)
    // .set("endedDate", `${endedDate}`);
    return this.httpClient.get<Page<Seance>>(Constants.URL + `head-class/seances/by-subject-field`, { params: params });
  }

  getSubjectFieldProgram(): Observable<Array<ProgrammeCourse>>{
    return this.httpClient.get<Array<ProgrammeCourse>>(Constants.URL + `head-class/subject-field`);
  }

  getMyCalendar(): Observable<Array<ProCalendar>>{
    return this.httpClient.get<Array<ProCalendar>>(Constants.URL + `head-class/pro-calendars`);
  }

  getSubjectField(id: string): Observable<SubjectField> {
    return this.httpClient.get<SubjectField>(Constants.URL + `department/subjects`);
  }

}
