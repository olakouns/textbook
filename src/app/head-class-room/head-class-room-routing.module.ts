import { SeancesParentComponent } from './programation/seances-parent/seances-parent.component';
import { ProgramationComponent } from './programation/programation.component';
import { MyTimesTableComponent } from './my-times-table/my-times-table.component';
import { HomeCroquisComponent } from './home-croquis/home-croquis.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "", redirectTo: 'programming' },
  { path: 'home', component: HomeCroquisComponent },
  { path: 'my-times-table', component: MyTimesTableComponent },
  { path: 'programming', component: ProgramationComponent },
  { path: 'seances/details/:id/:idProgramm', component: SeancesParentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeadClassRoomRoutingModule { }
